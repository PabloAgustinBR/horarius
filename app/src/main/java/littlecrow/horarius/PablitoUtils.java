package littlecrow.horarius;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class PablitoUtils {

    public static final String STRING = "string";

    public static ArrayList<String> jsonArrayToStringArrayList(JSONArray jsonArray, String conversionType){
        ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    switch (conversionType){
                        case STRING:
                            arrayList.add(jsonArray.getString(i));
                            break;
                        default:
                            return null;
                    }

                } catch (JSONException e) {
                    return null;
                }
            }
        return arrayList;
    }

    public static ArrayList<String> jsonArrayToStringArrayListNotNull(JSONArray jsonArray, String conversionType){
        ArrayList<String> arrayList = jsonArrayToStringArrayList(jsonArray, conversionType);
        if(arrayList == null){
            throw new NullPointerException(String.format("Something happen while converting JsonArray to ArrayList<%s>", conversionType));
        }
        return arrayList;
    }
}

package littlecrow.horarius.activities.result;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.applicationData.TravelDetailTimeData;
import littlecrow.horarius.data.searchData.ResultData;
import littlecrow.horarius.data.searchData.SearchData;
import littlecrow.horarius.data.searchData.TimeData;

public class ResultActivity extends AppCompatActivity {

    LinearLayout testLayout;

    int anchoRelativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        anchoRelativeLayout = getWindowManager().getDefaultDisplay().getWidth() - (getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin) * 2);

        testLayout = findViewById(R.id.layout_test);
        mostrarBusqueda();
    }


    public void mostrarBusqueda () {

        SearchData searchData = AccessData.getSearchData();
        List<ResultData> resultDataList = AccessData.getResultDataList();

            for (ResultData resultData : resultDataList) {

                LinearLayout linearLayoutHorizontalEmpresa = new LinearLayout(this);
                linearLayoutHorizontalEmpresa.setOrientation(LinearLayout.HORIZONTAL);


                //get resources

                TextView textViewEmpresa = new TextView(this);

                textViewEmpresa.setText("Empresa: ");
                textViewEmpresa.setTypeface(textViewEmpresa.getTypeface(), Typeface.BOLD);

                linearLayoutHorizontalEmpresa.addView(textViewEmpresa);

                TextView textViewEmpresaNombre = new TextView(this);
                textViewEmpresaNombre.setText(resultData.getCompanyData().getCompanyName());
                linearLayoutHorizontalEmpresa.addView(textViewEmpresaNombre);

                //linearLayoutTabla.addView(linearLayoutHorizontalEmpresa, layoutParamsLinearLayoutEmpresa);
                testLayout.addView(linearLayoutHorizontalEmpresa);

                LinearLayout linearLayoutHorizontalRecorrido = new LinearLayout(this);
                linearLayoutHorizontalRecorrido.setOrientation(LinearLayout.HORIZONTAL);

                TextView textViewRecorrido = new TextView(this);
                textViewRecorrido.setText("Recorrido: ");
                textViewRecorrido.setTypeface(textViewRecorrido.getTypeface(), Typeface.BOLD);
                linearLayoutHorizontalRecorrido.addView(textViewRecorrido);

                TextView textViewRecorridoNombre = new TextView(this);
                textViewRecorridoNombre.setText(resultData.getTravelData().getTravelName());
                linearLayoutHorizontalRecorrido.addView(textViewRecorridoNombre);

                testLayout.addView(linearLayoutHorizontalRecorrido);

                HorizontalScrollView horizontalScrollViewOrigenDestino = new HorizontalScrollView(this);

                testLayout.addView(horizontalScrollViewOrigenDestino);

                RelativeLayout relativeLayoutParaCentrarTabla = new RelativeLayout(this);
                relativeLayoutParaCentrarTabla.setMinimumWidth(anchoRelativeLayout);
                relativeLayoutParaCentrarTabla.setGravity(Gravity.CENTER);

                TableLayout tableLayoutOrigenDestino = new TableLayout(this);
                tableLayoutOrigenDestino.setMinimumWidth(anchoRelativeLayout);

                relativeLayoutParaCentrarTabla.addView(tableLayoutOrigenDestino);
                tableLayoutOrigenDestino.setGravity(Gravity.CENTER);

                //tableLayoutOrigenDestino.setBackgroundResource(R.drawable.background_tabla_origendestino_tabla);
                tableLayoutOrigenDestino.setGravity(Gravity.CENTER_VERTICAL);

                horizontalScrollViewOrigenDestino.addView(relativeLayoutParaCentrarTabla, HorizontalScrollView.LayoutParams.MATCH_PARENT,HorizontalScrollView.LayoutParams.WRAP_CONTENT);

                TableRow filaParadasOrigenDestino = new TableRow(this);
                //filaParadasOrigenDestino.setBackgroundResource(R.drawable.background_tabla_origendestino_titulo);
                filaParadasOrigenDestino.setGravity(Gravity.CENTER);


                tableLayoutOrigenDestino.addView(filaParadasOrigenDestino);

                TextView textViewComentarioAnteriorVacio = new TextView(this);
                textViewComentarioAnteriorVacio.setText("  ");

                filaParadasOrigenDestino.addView(textViewComentarioAnteriorVacio);


                List<String> stopList = resultData.getTravelDetailData().getStopList();
                int originIndex = stopList.indexOf(searchData.getSearchOrigin());
                int destinyIndex = stopList.indexOf(searchData.getSearchDestiny());

                for(String stop : resultData.getTravelDetailData().getStopList()){
                    TextView textViewParadaOrigen = new TextView(this);
                    textViewParadaOrigen.setTextColor(Color.WHITE);
                    textViewParadaOrigen.setGravity(Gravity.CENTER);
                    textViewParadaOrigen.setText("  " + stop + "  ");
                /*if (dtoDatosBusqueda.getEleccionOrigen().equals(dtoDatosBusqueda.getEleccionPasaPorOrigenDestino())) {
                    textViewParadaOrigen.setTypeface(textViewParadaOrigen.getTypeface(), Typeface.BOLD);
                }*/
                    filaParadasOrigenDestino.addView(textViewParadaOrigen);
                }

                TextView textViewComentarioSiguienteVacio = new TextView(this);
                textViewComentarioSiguienteVacio.setText("  ");

                filaParadasOrigenDestino.addView(textViewComentarioSiguienteVacio);


                for (TravelDetailTimeData travelDetailTimeData : resultData.getTravelDetailTimeDataList()) {

                    TableRow filaHorarioOrigenDestino = new TableRow(this);
                    filaHorarioOrigenDestino.setGravity(Gravity.CENTER);
                    //filaHorarioOrigenDestino.setBackgroundResource(R.drawable.background_tabla_origendestino_contenido);


                    tableLayoutOrigenDestino.addView(filaHorarioOrigenDestino);

                    TextView textViewComentarioAnterior = new TextView(this);
                    if (travelDetailTimeData.getCommentBefore() == null) {
                        textViewComentarioAnteriorVacio.setText("  ");
                    } else {
                        textViewComentarioAnterior.setText(travelDetailTimeData.getCommentBefore() + " ");
                    }

                    filaHorarioOrigenDestino.addView(textViewComentarioAnterior);

                    for(TimeData timeData : travelDetailTimeData.getTimeList()){
                        TextView textViewHorigenOrigen = new TextView(this);
                        textViewHorigenOrigen.setText(timeData.getTimeString());
                        textViewHorigenOrigen.setGravity(Gravity.CENTER);
                    /*if (dtoDatosBusqueda.getEleccionOrigen().equals(dtoDatosBusqueda.getEleccionPasaPorOrigenDestino())) {
                        textViewHorigenOrigen.setTypeface(textViewHorigenOrigen.getTypeface(), Typeface.BOLD);
                    }*/
                        filaHorarioOrigenDestino.addView(textViewHorigenOrigen);
                    }


                    TextView textViewComentarioSiguiente = new TextView(this);
                    if (travelDetailTimeData.getCommentAfter() == null) {
                        textViewComentarioSiguiente.setText("  ");
                    } else {
                        textViewComentarioSiguiente.setText(travelDetailTimeData.getCommentAfter());
                    }

                    filaHorarioOrigenDestino.addView(textViewComentarioSiguiente);

                }

            }

    }
}

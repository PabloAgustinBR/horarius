package littlecrow.horarius.activities.selection;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import littlecrow.horarius.R;
import littlecrow.horarius.datasource.HorariusDataSource;
import littlecrow.horarius.data.applicationData.LoadData;
import littlecrow.horarius.dayManagement.DayUtils;
import littlecrow.horarius.timeMagament.TimeUtils;

public class InitialActivity extends AppCompatActivity {

    HorariusDataSource dataSource;


    TextView textUpdatingDB;
    ProgressBar progressBarUpdatingDB;

    static final int SPLASH_SCREEN_TIME_WITHOUT_UPDATE = 1500;
    static final int SPLASH_SCREEN_TIME_WITH_UPDATE = 3500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_initial);

        textUpdatingDB =findViewById(R.id.activity_initial_text_updating_db);
        progressBarUpdatingDB =findViewById(R.id.activity_initial_progressbar_updating_db);

        textUpdatingDB.setVisibility(View.INVISIBLE);
        progressBarUpdatingDB.setVisibility(View.INVISIBLE);

        dataSource = HorariusDataSource.INSTANCE;
        try {
            dataSource.init(this);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        SharedPreferences preferences=getSharedPreferences("option_data", Context.MODE_PRIVATE);
        if(preferences.getBoolean("created_option",false)){
            SharedPreferences.Editor editor=preferences.edit();
            editor.putBoolean("created_option", true);
            editor.putBoolean("last_searchs_button", false);
            editor.putBoolean("automatic_lists", false);
            editor.putBoolean("tutorial_seen", false);
            editor.commit();
        }

        TimeUtils.setInitialTimes();
        DayUtils.setInitialDay();


        boolean necessaryUpdateDB = dataSource.necessaryUpdate();

            if(necessaryUpdateDB){

                textUpdatingDB.setVisibility(View.VISIBLE);
                progressBarUpdatingDB.setVisibility(View.VISIBLE);

                AssyncUpdateDB assyncUpdateDB = new AssyncUpdateDB();
                assyncUpdateDB.putContext(this);
                assyncUpdateDB.execute();

            }else {
                try {
                    LoadData.INSTANCE.loadData(this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Simulate a long loading process on application startup.
                Timer timer = new Timer();
                timer.schedule(task, SPLASH_SCREEN_TIME_WITHOUT_UPDATE);

            }


    }

    TimerTask task = new TimerTask() {
        @Override
        public void run() {

            // Start the next activity
            Intent mainIntent = new Intent(InitialActivity.this, MainActivity.class);

            startActivity(mainIntent);

            // Close the activity so the user won't able to go back this
            // activity pressing Back button
            finish();
        }
    };




    private class AssyncUpdateDB extends AsyncTask<String, Void, Object> {

        Context context;

        public void putContext(Context context){
            this.context =context;
        }

        protected Object doInBackground(String... args) {

            // This is where you would do all the work of downloading your data
            dataSource.updateDB();

            return true;
        }

        protected void onPostExecute(Object result) {
            // Pass the result data back to the main activity
            //MyActivity.this.data = result;
            try {
                LoadData.INSTANCE.loadData(this.context);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textUpdatingDB.setVisibility(View.INVISIBLE);
            progressBarUpdatingDB.setVisibility(View.INVISIBLE);
            Toast.makeText(context, R.string.update_db_finish, Toast.LENGTH_SHORT).show();
            Timer timer = new Timer();
            timer.schedule(task, SPLASH_SCREEN_TIME_WITH_UPDATE);



        }

    }
}

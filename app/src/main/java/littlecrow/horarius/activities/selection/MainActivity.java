package littlecrow.horarius.activities.selection;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import littlecrow.horarius.R;
import littlecrow.horarius.activities.result.ResultActivity;
import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.SearchDataUtils;
import littlecrow.horarius.data.frontData.MenuItemNavViewData;
import littlecrow.horarius.dayManagement.DaySelection;
import littlecrow.horarius.search.HorariusSearch;
import littlecrow.horarius.search.HorariusSearchFactory;
import littlecrow.horarius.timeMagament.HourSelection;

public class MainActivity extends AppCompatActivity {

    private Toolbar appbar;
    private DrawerLayout drawerLayout;
    private NavigationView navView;

    private Menu menuNavView;

    private static Button buttonSearch;


    private Map<Integer, MenuItemNavViewData> mapOptionsNavView= new HashMap<>();
    private int mapOptionNavViewGroupIndex = 0;
    private int mapOptionNavViewItemIndex = 0;


    private Calendar calendar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buttonSearch = findViewById(R.id.button_search);

        appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_launcher_background);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        /*
        //Eventos del Drawer Layout
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }
            @Override
            public void onDrawerOpened(View drawerView) {
            }
            @Override
            public void onDrawerClosed(View drawerView) {
            }
            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
        */

        navView = findViewById(R.id.navview);
        menuNavView = navView.getMenu();

        for(ApplicationData applicationData : AccessData.getApplicationDataMap().values()){
            SubMenu subMenuTest = menuNavView.addSubMenu(applicationData.getApplicationName());
            for(String searchType : applicationData.getSearchTypeList()){
                MenuItemNavViewData menuItemNavViewData = new MenuItemNavViewData();
                menuItemNavViewData.setApplication(applicationData.getApplicationName());
                menuItemNavViewData.setSearchType(searchType);
                menuItemNavViewData.setGroupId(mapOptionNavViewGroupIndex);
                menuItemNavViewData.setMenuItemId(mapOptionNavViewItemIndex);
                subMenuTest.add(menuItemNavViewData.getGroupId(),
                        menuItemNavViewData.getMenuItemId(),
                        Menu.NONE,
                        menuItemNavViewData.getSearchType());
                mapOptionsNavView.put(mapOptionNavViewItemIndex,menuItemNavViewData);
                mapOptionNavViewItemIndex++;
            }
            subMenuTest.setGroupCheckable(mapOptionNavViewGroupIndex,true,true);
            mapOptionNavViewGroupIndex++;
        }

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        boolean fragmentTransaction = false;
                        Fragment fragment = null;

                        Integer menuItemId = menuItem.getItemId();

                        MenuItemNavViewData menuItemNavViewData = mapOptionsNavView.get(menuItemId);

                        AccessData.getSearchData().setSearchApplication(menuItemNavViewData.getApplication());
                        AccessData.getSearchData().setSearchType(menuItemNavViewData.getSearchType());

                        switch (menuItemNavViewData.getSearchType()) {
                            case "company":
                                fragment = new SelectionByCompany();
                                fragmentTransaction = true;
                                break;
                            case "stops":
                                fragment = new SelectionByStops();
                                fragmentTransaction = true;
                                break;
                            case "favorite":
                                fragment = new Fragment3();
                                fragmentTransaction = true;
                                break;
                        }

                        if(fragmentTransaction) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .commit();

                            menuItem.setChecked(true);
                            getSupportActionBar().setTitle(menuItemNavViewData.getApplication() + " - " + menuItemNavViewData.getSearchType());
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });



        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HorariusSearch horariusSearch = HorariusSearchFactory.INSTANCE.getHorariusSearch();
                if(horariusSearch != null){
                    horariusSearch.horariusSearch();
                    if("stops".equals(AccessData.getSearchData().getSearchType())){
                        test();
                    }
                    Toast.makeText(v.getContext(),"factory creo: " + horariusSearch.getClass().getSimpleName(),Toast.LENGTH_SHORT).show();
                    System.out.println("Aca pablin " + AccessData.getResultDataList());
                }
            }
        });


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_hour_selection, new HourSelection())
                .commit();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_day_selection, new DaySelection())
                .commit();

        buttonSearchEnabler();

    }

    public void test(){
        Intent i = new Intent(this, ResultActivity.class);
        startActivity(i);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public static void buttonSearchEnabler(){
        buttonSearch.setEnabled(SearchDataUtils.shouldEnableSearchButton());
    }
}
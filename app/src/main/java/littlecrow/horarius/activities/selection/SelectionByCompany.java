package littlecrow.horarius.activities.selection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import littlecrow.horarius.App;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.applicationData.TravelDetailData;

public class SelectionByCompany extends Fragment {

    private Button selectionByCompanyButtonCompany;
    private Button selectionByCompanyButtonTravel;
    private Button selectionByCompanyButtonStop;

    public SelectionByCompany() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.selection_by_company, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        selectionByCompanyButtonCompany = getView().findViewById(R.id.selection_by_company_button_company);
        selectionByCompanyButtonTravel = getView().findViewById(R.id.selection_by_company_button_travel);
        selectionByCompanyButtonStop = getView().findViewById(R.id.selection_by_company_button_stop);



        setButtonsText();
        selectionByCompanyButtonCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    inflateCompanySelection(view, v);
                }catch (Exception e){
                    AccessData.getSearchData().setSearchCompany(null);
                }finally {
                    setButtonsText();
                }
            }
        });

        selectionByCompanyButtonTravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    inflateTravelSelection(view, v);
                }catch (Exception e){
                    AccessData.getSearchData().setSearchCompany(null);
                }finally {
                    setButtonsText();
                }
            }
        });
        selectionByCompanyButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    inflateStopSelection(view, v);
                }catch (Exception e){
                    AccessData.getSearchData().setSearchCompany(null);
                }finally {
                    setButtonsText();
                }
            }
        });


    }

    private void inflateCompanySelection(final View view, final View v){

        Set<String> companyListAux = AccessData.getApplicationData(AccessData.getSearchData().getSearchApplication())
                .getCompanyDataMap().keySet();

        final List<String> companyList= Arrays.asList(companyListAux.toArray(new String[0]));

        if(companyList.size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            builder.setTitle(App.getResourses().getString(R.string.selection_by_company_text_company_description));

            builder.setItems((CharSequence[]) companyList.toArray(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // the user clicked on colors[which]
                    AccessData.getSearchData().setSearchCompany(companyList.get(which));
                    AccessData.getSearchData().setSearchTravel(null);
                    AccessData.getSearchData().setSearchStop(null);

                    inflateTravelSelection(view,v);
                    setButtonsText();

                }
            });

            builder.show();
        }else{
            Toast.makeText(view.getContext(),App.getResourses().getString(R.string.selection_list_empty), Toast.LENGTH_SHORT).show();
            AccessData.getSearchData().setSearchCompany(null);
        }
        setButtonsText();



    }

    private void inflateTravelSelection(final View view, final View v){

        Set<String> travelListAux = AccessData.getApplicationData(AccessData.getSearchData().getSearchApplication())
                .getCompanyDataMap().get(AccessData.getSearchData().getSearchCompany())
                .getTravelDataMap().keySet();

        final List<String> travelList= Arrays.asList(travelListAux.toArray(new String[0]));

        if(travelList.size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            builder.setTitle(App.getResourses().getString(R.string.selection_by_company_text_travel_description));

            builder.setItems((CharSequence[]) travelList.toArray(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // the user clicked on colors[which]
                    AccessData.getSearchData().setSearchTravel(travelList.get(which));
                    AccessData.getSearchData().setSearchStop(null);

                    inflateStopSelection(view,v);
                    setButtonsText();

                }
            });

            builder.show();
        }else{
            Toast.makeText(view.getContext(),App.getResourses().getString(R.string.selection_list_empty), Toast.LENGTH_SHORT).show();
            AccessData.getSearchData().setSearchTravel(null);
        }

        setButtonsText();


    }

    private void inflateStopSelection(View view, View v) {
        Set<String> stopListAux = new HashSet<>();
        for(TravelDetailData travelDetailData : AccessData.getApplicationData(AccessData.getSearchData().getSearchApplication())
                .getCompanyDataMap().get(AccessData.getSearchData().getSearchCompany())
                .getTravelDataMap().get(AccessData.getSearchData().getSearchTravel())
                .getTravelDetailDataList()){
            stopListAux.addAll(travelDetailData.getStopList());
        }

        final List<String> stopList= Arrays.asList(stopListAux.toArray(new String[0]));

        if(stopList.size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            builder.setTitle(App.getResourses().getString(R.string.selection_by_company_text_stop_description));
            builder.setItems((CharSequence[]) stopList.toArray(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // the user clicked on colors[which]
                    AccessData.getSearchData().setSearchStop(stopList.get(which));
                    setButtonsText();

                }
            });

            builder.show();
        }else{
            Toast.makeText(view.getContext(),App.getResourses().getString(R.string.selection_list_empty), Toast.LENGTH_SHORT).show();
            AccessData.getSearchData().setSearchStop(null);
        }

        setButtonsText();


    }

    public void setButtonsText(){

        String company = AccessData.getSearchData().getSearchCompany();
        String travel = company != null ? AccessData.getSearchData().getSearchTravel() : null;
        String stop = company != null && travel != null ? AccessData.getSearchData().getSearchStop() : null;

        selectionByCompanyButtonCompany.setText( company != null ? company : App.getResourses().getString(R.string.selection_by_company_text_company));
        selectionByCompanyButtonTravel.setText(travel != null ? travel : App.getResourses().getString(R.string.selection_by_company_text_travel));
        selectionByCompanyButtonStop.setText(stop != null ? stop : App.getResourses().getString(R.string.selection_by_company_text_stop));

        selectionByCompanyButtonTravel.setEnabled(company != null);
        selectionByCompanyButtonStop.setEnabled(company != null && travel != null);


        MainActivity.buttonSearchEnabler();

    }
}

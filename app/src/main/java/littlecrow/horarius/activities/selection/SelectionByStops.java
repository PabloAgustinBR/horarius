package littlecrow.horarius.activities.selection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import littlecrow.horarius.App;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.AccessDataUtils;

public class SelectionByStops extends Fragment {

    private Button selectionByStopsButtonOrigin;
    private Button selectionByStopsButtonDestiny;


    public SelectionByStops() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.selection_by_stops, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        selectionByStopsButtonOrigin = getView().findViewById(R.id.selection_by_stops_button_origin);
        selectionByStopsButtonDestiny = getView().findViewById(R.id.selection_by_stops_button_destiny);


        setButtonsText();
        selectionByStopsButtonOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    inflateOriginSelection(view, v);
                }catch (Exception e){
                    AccessData.getSearchData().setSearchOrigin(null);
                }finally {
                    setButtonsText();
                }
            }
        });

        selectionByStopsButtonDestiny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    inflateDestinySelection(view, v);
                }catch (Exception e){
                    AccessData.getSearchData().setSearchDestiny(null);
                }finally {
                    setButtonsText();
                }
            }
        });

    }

    private void inflateOriginSelection(final View view, final View v){

        Set<String> originListAux = AccessDataUtils.getAllStopList();

        final List<String> originList= Arrays.asList(originListAux.toArray(new String[0]));

        if(originList.size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            builder.setTitle(App.getResourses().getString(R.string.selection_by_stops_text_origin_description));

            builder.setItems((CharSequence[]) originList.toArray(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // the user clicked on colors[which]
                    AccessData.getSearchData().setSearchOrigin(originList.get(which));
                    AccessData.getSearchData().setSearchDestiny(null);

                    inflateDestinySelection(view,v);
                    setButtonsText();

                }
            });

            builder.show();
        }else{
            Toast.makeText(view.getContext(),App.getResourses().getString(R.string.selection_list_empty), Toast.LENGTH_SHORT).show();
            AccessData.getSearchData().setSearchCompany(null);
        }
        setButtonsText();



    }

    private void inflateDestinySelection(final View view, final View v){

        Set<String> destinyListAux = AccessDataUtils.getStopListForOneStop(AccessData.getSearchData().getSearchOrigin());

        final List<String> destinyList= Arrays.asList(destinyListAux.toArray(new String[0]));

        if(destinyList.size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            builder.setTitle(App.getResourses().getString(R.string.selection_by_stops_text_destiny_description));

            builder.setItems((CharSequence[]) destinyList.toArray(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // the user clicked on colors[which]
                    AccessData.getSearchData().setSearchDestiny(destinyList.get(which));

                    setButtonsText();

                }
            });

            builder.show();
        }else{
            Toast.makeText(view.getContext(),App.getResourses().getString(R.string.selection_list_empty), Toast.LENGTH_SHORT).show();
            AccessData.getSearchData().setSearchTravel(null);
        }

        setButtonsText();


    }

    public void setButtonsText(){

        String origin = AccessData.getSearchData().getSearchOrigin();
        String destiny = origin != null ? AccessData.getSearchData().getSearchDestiny() : null;

        selectionByStopsButtonOrigin.setText( origin != null ? origin : App.getResourses().getString(R.string.selection_by_stops_text_origin));
        selectionByStopsButtonDestiny.setText(destiny != null ? destiny : App.getResourses().getString(R.string.selection_by_stops_text_destiny));

        selectionByStopsButtonDestiny.setEnabled(origin != null);

        MainActivity.buttonSearchEnabler();

    }
}

package littlecrow.horarius.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.searchData.ResultData;
import littlecrow.horarius.data.searchData.SearchData;

public class AccessData {
    static private Map<String, ApplicationData> applicationDataList = new HashMap<>();

    static private SearchData searchData = new SearchData();

    static private List<ResultData> resultDataList = new ArrayList<>();

    static public Map<String, ApplicationData> getApplicationDataMap() {
        return applicationDataList;
    }

    static public ApplicationData getApplicationData(String applicationName) {
        return applicationDataList.get(applicationName);
    }

    static public void addApplicationData(ApplicationData applicationData) {
        applicationDataList.put(applicationData.getApplicationName(), applicationData);
    }

    public static SearchData getSearchData() {
        return searchData;
    }

    public static void setSearchData(SearchData searchData) {
        AccessData.searchData = searchData;
    }

    public static List<ResultData> getResultDataList() {
        return resultDataList;
    }

    public static void setResultDataList(List<ResultData> resultDataList) {
        AccessData.resultDataList = resultDataList;
    }

    public static void addResultData(ResultData resultData) {
        AccessData.resultDataList.add(resultData);
    }
}

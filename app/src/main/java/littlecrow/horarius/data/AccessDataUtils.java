package littlecrow.horarius.data;

import java.util.HashSet;
import java.util.Set;

import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.applicationData.CompanyData;
import littlecrow.horarius.data.applicationData.TravelData;
import littlecrow.horarius.data.applicationData.TravelDetailData;

public class AccessDataUtils {

    public static Set<String> getAllStopList(){
        Set<String> allStops = new HashSet<>();
        for (String applicationName: AccessData.getApplicationDataMap().keySet()) {
            ApplicationData applicationData = AccessData.getApplicationData(applicationName);
            for (String companyName : applicationData.getCompanyDataMap().keySet()){
                CompanyData companyData = applicationData.getCompanyData(companyName);
                for (String travelName : companyData.getTravelDataMap().keySet()){
                    TravelData travelData = companyData.getTravelData(travelName);
                    for (TravelDetailData travelDetailData : travelData.getTravelDetailDataList()) {
                        allStops.addAll(travelDetailData.getStopList());
                    }
                }
            }
        }
        return allStops;
    }

    public static Set<String> getStopListForOneStop(String stopReference){
        Set<String> stopsList = new HashSet<>();
        for (String applicationName: AccessData.getApplicationDataMap().keySet()) {
            ApplicationData applicationData = AccessData.getApplicationData(applicationName);
            for (String companyName : applicationData.getCompanyDataMap().keySet()){
                CompanyData companyData = applicationData.getCompanyData(companyName);
                for (String travelName : companyData.getTravelDataMap().keySet()){
                    TravelData travelData = companyData.getTravelData(travelName);
                    for (TravelDetailData travelDetailData : travelData.getTravelDetailDataList()) {
                        if(travelDetailData.getStopList().contains(stopReference)){
                            stopsList.addAll(travelDetailData.getStopList());
                        }
                    }
                }
            }
        }
        stopsList.remove(stopReference);
        return stopsList;
    }
}

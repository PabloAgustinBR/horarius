package littlecrow.horarius.data.applicationData;

import android.database.Cursor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationData {
    private Integer applicationId;
    private String applicationName;
    private List<String> searchTypeList;
    private Map<String, CompanyData> companyDataMap = new HashMap<>();

    public ApplicationData(Cursor cursorApplicationData){
        this.applicationId = cursorApplicationData.getInt(0);
        this.applicationName = cursorApplicationData.getString(1);
        this.searchTypeList = Arrays.asList(cursorApplicationData.getString(2).split("\\s*,\\s*"));
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public List<String> getSearchTypeList() {
        return searchTypeList;
    }

    public void setSearchTypeList(List<String> searchTypeList) {
        this.searchTypeList = searchTypeList;
    }

    public Map<String, CompanyData> getCompanyDataMap() {
        return companyDataMap;
    }

    public CompanyData getCompanyData(String companyName) {
        return companyDataMap.get(companyName);
    }

    public void setCompanyDataMap(Map<String, CompanyData> companyDataMap) {
        this.companyDataMap = companyDataMap;
    }

    public void addCompanyData(CompanyData companyData) {
        this.companyDataMap.put(companyData.getCompanyName(),companyData);
    }
}

package littlecrow.horarius.data.applicationData;

import android.database.Cursor;

import java.util.HashMap;
import java.util.Map;

public class CompanyData {
    private Integer companyId;
    private String companyName;
    private Map<String, TravelData> travelDataMap = new HashMap<>();

    public CompanyData(Cursor companyData){
        this.companyId = companyData.getInt(0);
        this.companyName = companyData.getString(1);
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Map<String, TravelData> getTravelDataMap() {
        return travelDataMap;
    }
    public TravelData getTravelData(String travelName) {
        return travelDataMap.get(travelName);
    }

    public void setTravelDataMap(Map<String, TravelData> travelDataMap) {
        this.travelDataMap = travelDataMap;
    }

    public void addTravelData(TravelData travelData) {
        this.travelDataMap.put(travelData.getTravelName(), travelData);
    }
}

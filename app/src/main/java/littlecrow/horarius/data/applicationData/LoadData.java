package littlecrow.horarius.data.applicationData;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import org.json.JSONException;

import java.io.IOException;

import littlecrow.horarius.datasource.HorariusDataSource;
import littlecrow.horarius.data.AccessData;

public enum LoadData {

    INSTANCE;

     HorariusDataSource dataSource;


    public void loadData(Context context) throws JSONException {
        dataSource = HorariusDataSource.INSTANCE;
        try {
            dataSource.init(context);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadApplicationData();
    }

    private void loadApplicationData() throws JSONException {
        Cursor cursorApplication= dataSource.getApplicationList();
        while(cursorApplication.moveToNext()) {
            ApplicationData applicationData = new ApplicationData(cursorApplication);


            Cursor cursorCompany = dataSource.getCompanyListByApplication(applicationData.getApplicationId());
            while(cursorCompany.moveToNext()) {

                    CompanyData companyData = new CompanyData(cursorCompany);

                    Cursor cursorTravel = dataSource.getTravelListByCompany(companyData.getCompanyId());
                    while(cursorTravel.moveToNext()){
                        TravelData travelData = new TravelData(cursorTravel);

                        Cursor cursorTravelDetail = dataSource.getTravelDetailListByTravel(travelData.getTravelId());
                        while(cursorTravelDetail.moveToNext()){
                            TravelDetailData travelDetailData = new TravelDetailData(cursorTravelDetail);
                            travelData.addTravelDetailData(travelDetailData);
                        }
                        companyData.addTravelData(travelData);
                    }
                applicationData.addCompanyData(companyData);
            }

            AccessData.addApplicationData(applicationData);
        }
    }
}

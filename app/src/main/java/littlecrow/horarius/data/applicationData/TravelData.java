package littlecrow.horarius.data.applicationData;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class TravelData {
    private Integer travelId;
    private String travelName;
    private List<TravelDetailData> travelDetailDataList = new ArrayList<>();

    public TravelData (Cursor travelData){
        this.travelId = travelData.getInt(0);
        this.travelName = travelData.getString(1);
    }

    public Integer getTravelId() {
        return travelId;
    }

    public void setTravelId(Integer travelId) {
        this.travelId = travelId;
    }

    public String getTravelName() {
        return travelName;
    }

    public void setTravelName(String travelName) {
        this.travelName = travelName;
    }

    public List<TravelDetailData> getTravelDetailDataList() {
        return travelDetailDataList;
    }

    public void setTravelDetailDataList(List<TravelDetailData> travelDetailDataList) {
        this.travelDetailDataList = travelDetailDataList;
    }

    public void addTravelDetailData(TravelDetailData travelDetailData) {
        this.travelDetailDataList.add(travelDetailData);
    }
}

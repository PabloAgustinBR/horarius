package littlecrow.horarius.data.applicationData;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import littlecrow.horarius.PablitoUtils;

public class TravelDetailData {
    private Integer travelDetailId;
    private List<String> stopList = new ArrayList<>();
    private List<String> seasonList = new ArrayList<>();
    private List<String> dayList = new ArrayList<>();
    private List<TravelDetailTimeData> travelDetailTimeDataList = new ArrayList<>();


    public TravelDetailData (Cursor travelDetailData) throws JSONException {
        this.travelDetailId = travelDetailData.getInt(0);

        this.stopList.addAll(PablitoUtils.jsonArrayToStringArrayListNotNull(new JSONArray(travelDetailData.getString(1)), PablitoUtils.STRING));
        this.seasonList.addAll(PablitoUtils.jsonArrayToStringArrayListNotNull(new JSONArray(travelDetailData.getString(2)), PablitoUtils.STRING));
        this.dayList.addAll(PablitoUtils.jsonArrayToStringArrayListNotNull(new JSONArray(travelDetailData.getString(3)), PablitoUtils.STRING));

        JSONArray timeJsonArray = new JSONArray(travelDetailData.getString(4));
        for (int i = 0; i < timeJsonArray.length(); i++) {
            this.travelDetailTimeDataList.add(new TravelDetailTimeData(timeJsonArray.optJSONObject(i)));
        }
        /*long arrayInitial = System.currentTimeMillis();
        Collections.addAll(this.stopList, travelDetailData.getString(1).replace("[","").replace("]","").split(DBConstants.Data.travelDetailStopListDelimiter));
        long arrayFinal = System.currentTimeMillis();
        System.out.println("aca pablito, tiempo array : " + (arrayFinal - arrayInitial));

        Collections.addAll(this.seasonList, travelDetailData.getString(2).split(DBConstants.Data.travelDetailSeasonListDelimiter));

        Collections.addAll(this.dayList, travelDetailData.getString(3).split(DBConstants.Data.travelDetailDayListDelimiter));

        for (String timeData: travelDetailData.getString(4).split(DBConstants.Data.travelDetailTimeListDelimiter)) {
            this.travelDetailTimeDataList.add(new TravelDetailTimeData(timeData));
        }*/

    }

    public List<String> getDayList() {
        return dayList;
    }

    public void setDayList(List<String> dayList) {
        this.dayList = dayList;
    }

    public List<String> getStopList() {
        return stopList;
    }

    public void setStopList(List<String> stopList) {
        this.stopList = stopList;
    }

    public List<TravelDetailTimeData> getTravelDetailTimeDataList() {
        return travelDetailTimeDataList;
    }

    public void setTravelDetailTimeDataList(List<TravelDetailTimeData> travelDetailTimeDataList) {
        this.travelDetailTimeDataList = travelDetailTimeDataList;
    }

    public void addTimeData(TravelDetailTimeData travelDetailTimeData) {
        this.travelDetailTimeDataList.add(travelDetailTimeData);
    }
}

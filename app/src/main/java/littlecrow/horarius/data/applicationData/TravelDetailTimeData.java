package littlecrow.horarius.data.applicationData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import littlecrow.horarius.datasource.DBConstants;
import littlecrow.horarius.data.searchData.TimeData;

public class TravelDetailTimeData {
    private String commentBefore;
    private String commentAfter;
    private List<TimeData> timeList = new ArrayList<>();

    public TravelDetailTimeData (String timeData){
        String[] timeDataAux = timeData.split(DBConstants.Data.travelDetailTimeCommentListDelimiter);
        if(timeDataAux.length == 1){
            for(String time : timeDataAux[0].split(DBConstants.Data.travelDetailTimeInsideListDelimiter)){
                timeList.add(new TimeData(time));
            }
        }else{
            this.commentBefore = timeDataAux[0];
            for(String time : timeDataAux[1].split(DBConstants.Data.travelDetailTimeInsideListDelimiter)){
                timeList.add(new TimeData(time));
            }            this.commentAfter = timeDataAux[2];
        }
    }

    public TravelDetailTimeData(JSONObject timeData){
        this.commentBefore = timeData.optString("comment_before");
        this.commentAfter = timeData.optString("comment_after");
        JSONArray timesJsonArray = timeData.optJSONArray("time_list");
        if(timesJsonArray != null){
            for (int i = 0; i < timesJsonArray.length(); i++) {
                this.timeList.add(new TimeData(timesJsonArray.optJSONObject(i)));
            }
        }
    }

    public String getCommentBefore() {
        return commentBefore;
    }

    public void setCommentBefore(String commentBefore) {
        this.commentBefore = commentBefore;
    }

    public String getCommentAfter() {
        return commentAfter;
    }

    public void setCommentAfter(String commentAfter) {
        this.commentAfter = commentAfter;
    }

    public List<TimeData> getTimeList() {
        return timeList;
    }

}

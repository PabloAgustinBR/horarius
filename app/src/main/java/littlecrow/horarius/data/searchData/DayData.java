package littlecrow.horarius.data.searchData;

import java.util.Calendar;

import littlecrow.horarius.dayManagement.DayUtils;

public class DayData {

    String dayKey;
    String dayData;
    String dayNumber;

    public DayData(Calendar calendar){

        this.dayKey = DayUtils.getDayKey(calendar);
        this.dayData = DayUtils.getDay(this.dayKey);

        this.dayNumber = formatDayNumber(calendar);
    }

    public DayData(Integer day, Integer month, Integer year){

        this.dayNumber = formatDayNumber(day, month, year);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        this.dayKey = DayUtils.getDayKey(calendar);
        this.dayData = DayUtils.getDay(this.dayKey);
    }

    public DayData(String dayData){
        this.dayData = dayData;
    }

    private String formatDayNumber(Calendar calendar){
        return formatDayNumber(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    }

    private String formatDayNumber(Integer day, Integer month, Integer year){
        return SearchDataUtils.getStringNumberWithZero(day) + "/" + SearchDataUtils.getStringNumberWithZero(month + 1) + "/" + year;
    }

    public String getDayData() {
        return dayData;
    }

    public void setDayData(String dayData) {
        this.dayData = dayData;
    }

    public String getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(String dayNumber) {
        this.dayNumber = dayNumber;
    }

    public void setDayNumber(Calendar calendar){
        this.dayNumber = formatDayNumber(calendar);
    }

    public void setDayNumber(Integer day, Integer month, Integer year){
        this.dayNumber = formatDayNumber(day,month,year);
    }

    public String getDayKey() {
        return dayKey;
    }

    public void setDayKey(String dayKey) {
        this.dayKey = dayKey;
    }
}

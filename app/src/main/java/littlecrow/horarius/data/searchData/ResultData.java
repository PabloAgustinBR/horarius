package littlecrow.horarius.data.searchData;

import java.util.ArrayList;
import java.util.List;

import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.applicationData.CompanyData;
import littlecrow.horarius.data.applicationData.TravelData;
import littlecrow.horarius.data.applicationData.TravelDetailData;
import littlecrow.horarius.data.applicationData.TravelDetailTimeData;

public class ResultData {
    private ApplicationData applicationData = null;
    private CompanyData companyData = null;
    private TravelData travelData = null;
    private TravelDetailData travelDetailData = null;
    private List<TravelDetailTimeData> travelDetailTimeDataList = new ArrayList<>();

    public ApplicationData getApplicationData() {
        return applicationData;
    }

    public void setApplicationData(ApplicationData applicationData) {
        this.applicationData = applicationData;
    }

    public CompanyData getCompanyData() {
        return companyData;
    }

    public void setCompanyData(CompanyData companyData) {
        this.companyData = companyData;
    }

    public TravelData getTravelData() {
        return travelData;
    }

    public void setTravelData(TravelData travelData) {
        this.travelData = travelData;
    }

    public TravelDetailData getTravelDetailData() {
        return travelDetailData;
    }

    public void setTravelDetailData(TravelDetailData travelDetailData) {
        this.travelDetailData = travelDetailData;
    }

    public List<TravelDetailTimeData> getTravelDetailTimeDataList() {
        return travelDetailTimeDataList;
    }

    public void setTravelDetailTimeDataList(List<TravelDetailTimeData> travelDetailTimeDataList) {
        this.travelDetailTimeDataList = travelDetailTimeDataList;
    }
    public void addTravelDetailTimeData(TravelDetailTimeData travelDetailTimeData) {
        this.travelDetailTimeDataList.add(travelDetailTimeData);
    }
}

package littlecrow.horarius.data.searchData;

public class SearchData {

    static final Integer TYPE_COMPANY_TRAVEL = 1;
    static final Integer TYPE_ORIGIN_DESTINY = 2;

    String searchApplication;
    String searchType;

    String searchCompany;
    String searchTravel;
    String searchStop;
    String searchOrigin;
    String searchDestiny;
    TimeData timeBegin;
    TimeData timeEnd;
    DayData dayData;

    public SearchData() {
    }

    public String getSearchApplication() {
        return searchApplication;
    }

    public void setSearchApplication(String searchApplication) {
        this.searchApplication = searchApplication;
    }

    public String getSearchCompany() {
        return searchCompany;
    }

    public void setSearchCompany(String searchCompany) {
        this.searchCompany = searchCompany;
    }

    public String getSearchTravel() {
        return searchTravel;
    }

    public void setSearchTravel(String searchTravel) {
        this.searchTravel = searchTravel;
    }

    public String getSearchStop() {
        return searchStop;
    }

    public void setSearchStop(String searchStop) {
        this.searchStop = searchStop;
    }

    public String getSearchOrigin() {
        return searchOrigin;
    }

    public void setSearchOrigin(String searchOrigin) {
        this.searchOrigin = searchOrigin;
    }

    public String getSearchDestiny() {
        return searchDestiny;
    }

    public void setSearchDestiny(String searchDestiny) {
        this.searchDestiny = searchDestiny;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public TimeData getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(TimeData timeBegin) {
        this.timeBegin = timeBegin;

    }

    public TimeData getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(TimeData timeEnd) {
        this.timeEnd = timeEnd;

    }

    public DayData getDayData() {
        return dayData;
    }

    public void setDayData(DayData dayData) {
        this.dayData = dayData;
    }
}

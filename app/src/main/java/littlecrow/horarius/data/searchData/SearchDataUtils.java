package littlecrow.horarius.data.searchData;

import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.timeMagament.TimeConstants;
import littlecrow.horarius.timeMagament.TimeUtils;

public class SearchDataUtils {

    public static String getStringNumberWithZero(Integer number){
        if(number<10){
            return  "0".concat(number.toString());
        }else{
            return number.toString();
        }
    }

    public static boolean shouldEnableSearchButton(){

        SearchData searchData = AccessData.getSearchData();
        boolean isTimeCorrect = TimeUtils.consistentHourChecker() == TimeConstants.TIME_CORRECT;
        if(searchData.getSearchType() == null){
            return false;
        }
        switch (searchData.getSearchType()){
            case "company":
                return searchData.getSearchCompany() != null &&
                        searchData.getSearchTravel() != null &&
                        searchData.getSearchStop() != null &&
                        searchData.getDayData() != null &&
                        isTimeCorrect;
            case "stops":
                return searchData.getSearchOrigin() != null &&
                        searchData.getSearchDestiny() != null &&
                        searchData.getDayData() != null &&
                        isTimeCorrect;
                default:
                    return false;
        }
    }
}

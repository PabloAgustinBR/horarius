package littlecrow.horarius.data.searchData;

import org.json.JSONObject;

import java.util.Calendar;
import static littlecrow.horarius.timeMagament.TimeConstants.*;


public class TimeData {

    private String commentBefore = "";
    private String commentAfter = "";
    private Integer hour = 0;
    private Integer minute = 0;
    private String timeString;
    private Integer comparisionHour;


    public TimeData(TimeData timeData){
        this.hour = timeData.hour;
        this.minute = timeData.minute;
        this.timeString = timeData.timeString;
        this.comparisionHour = timeData.comparisionHour;
        this.commentAfter = timeData.commentAfter;
        this.commentBefore = timeData.commentBefore;
    }

    public TimeData(JSONObject timeInfo){

        this.commentAfter = timeInfo.optString("comment_after");
        this.commentBefore = timeInfo.optString("comment_before");
        String time = timeInfo.optString("time");

        String[] timeAux = time.split(":");

        String hourAux = timeAux[0].trim();
        String minAux = timeAux[1].trim();
        
        setHour(Integer.valueOf(hourAux));
        setMinute(Integer.valueOf(minAux));
        setComparitionHour();
        setTimeString();
    }

    public TimeData(String time){

        String[] timeAux = time.split(":");

        String hourAux = timeAux[0].trim();
        String minAux = timeAux[1].trim();

        setHour(Integer.valueOf(hourAux));
        setMinute(Integer.valueOf(minAux));
        setComparitionHour();
        setTimeString();
    }

    public TimeData(Integer hour, Integer minute){

        setHour(hour);
        setMinute(minute);
        setComparitionHour();
        setTimeString();

    }

    public TimeData(Calendar calendar){
        Integer hour = calendar.get(Calendar.HOUR_OF_DAY);
        Integer minute = calendar.get(Calendar.MINUTE);

        setHour(hour);
        setMinute(minute);
        setComparitionHour();
        setTimeString();
    }

    public void setHour(Integer hour){

        this.hour = checkAndGetHour(hour);

        setComparitionHour();
        setTimeString();

    }

    public void setMinute(Integer minute){

        this.minute = checkAndGetMinute(minute);
        setTimeString();

    }

    public Integer getHour(){
        return this.hour;
    }

    public Integer getMinute(){
        return this.minute;
    }
    
    public Integer getComparisionHour() {
        return comparisionHour;
    }

    public String getCommentBefore() {
        return commentBefore;
    }

    public void setCommentBefore(String commentBefore) {
        this.commentBefore = commentBefore;
    }

    public String getCommentAfter() {
        return commentAfter;
    }

    public void setCommentAfter(String commentAfter) {
        this.commentAfter = commentAfter;
    }

    public boolean isTimeDataBefore(TimeData timeDataReference) {
        return this.getComparisionHour() < timeDataReference.getComparisionHour()
                || (this.getComparisionHour().equals(timeDataReference.getComparisionHour())
                && this.getMinute() < timeDataReference.getMinute());
    }

    public boolean isTimeDataAfter(TimeData timeDataReference) {
        return this.getComparisionHour() > timeDataReference.getComparisionHour()
                || (this.getComparisionHour().equals(timeDataReference.getComparisionHour())
                && this.getMinute() > timeDataReference.getMinute());
    }

    public boolean isTimeDataBeforeOrEquals(TimeData timeDataReference) {
        return this.getComparisionHour() < timeDataReference.getComparisionHour()
                || (this.getComparisionHour().equals(timeDataReference.getComparisionHour())
                && this.getMinute() < timeDataReference.getMinute())
                || isTimeDataEquals(timeDataReference);
    }

    public boolean isTimeDataAfterOrEquals(TimeData timeDataReference) {
        return this.getComparisionHour() > timeDataReference.getComparisionHour()
                || (this.getComparisionHour().equals(timeDataReference.getComparisionHour())
                && this.getMinute() > timeDataReference.getMinute())
                || isTimeDataEquals(timeDataReference);
    }

    public boolean isTimeDataEquals(TimeData timeDataReference){
        return this.getComparisionHour().equals(timeDataReference.getComparisionHour())
                && this.getMinute().equals(timeDataReference.getMinute());
    }

    private void setComparitionHour() {
        if(this.hour < REAL_MINIMUM_HOUR){
            this.comparisionHour = this.hour + 24;
        }else{
            this.comparisionHour = this.hour;
        }
    }

    public String getTimeString() {
        return timeString;
    }

    private void setTimeString() {
        this.timeString = SearchDataUtils.getStringNumberWithZero(this.hour)
        .concat(":")
        .concat(SearchDataUtils.getStringNumberWithZero(this.minute));
    }

    public Integer checkAndGetHour(Integer hour){
        if(hour >= 0 && hour < 24){
            return hour;
        }else if (hour > 24){
            return checkAndGetHour(hour - 24);
        }else if (hour < 0){
            return checkAndGetHour(hour + 24);
        }
        return 0;
    }

    public Integer checkAndGetMinute(Integer minute){
        if(minute >= 0 && minute < 60){
            return minute;
        }else if(minute > 60){
            setHour(this.hour + 1);
            return checkAndGetMinute(minute - 60);
        }else if(minute < 0){
            setHour(this.hour - 1);
            return checkAndGetMinute(minute + 60);
        }
        return 0;
    }

    public void add (int type, Integer value){
        if(type == HOUR){
            setHour(this.hour + value);
        }else if (type == MINUTE){
            setMinute(this.minute + value);
        }
    }
}

package littlecrow.horarius.datasource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 137;
    public static final String DATABASE_NAME = "horarius.db";

    private Boolean requiredUpdate = false;

    private Context mContext;

    public AdminSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;



    }

    public void createDataBase() throws IOException {
        Log.i(this.getClass().toString(), "Creando base de datos");

        File pathFile = mContext.getDatabasePath(DATABASE_NAME);
        boolean dbExist = checkDataBase(pathFile.getAbsolutePath());

        if (!dbExist) {
            this.getReadableDatabase();
            Toast.makeText(mContext,"Instalando Base de Data.",Toast.LENGTH_LONG).show();
            try {
                copyDataBase(pathFile);
            } catch (IOException e) {
                // Error copying database
            }

        }
        if (requiredUpdate) {
            requiredUpdate = false;
            this.getReadableDatabase();
            try {
                copyDataBase(pathFile);
                //Toast.makeText(mContext, "Base de Data actualizada con exito.", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                // Error copying database
            }
        }

    }

    private boolean checkDataBase(String path) {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
        } catch (Exception e) {
            // Database doesn't exist
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void copyDataBase(File pathFile) throws IOException {
        InputStream myInput = mContext.getAssets().open(DATABASE_NAME);
        OutputStream myOutput = new FileOutputStream(pathFile);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        requiredUpdate = true;

    }

    public Boolean getRequiredUpdate() {
        return requiredUpdate;
    }

    public void setRequiredUpdate(Boolean requiredUpdate) {
        this.requiredUpdate = requiredUpdate;
    }



}

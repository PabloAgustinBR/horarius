package littlecrow.horarius.datasource;

public class DBConstants {

    public static class Data {

        public static final String applicationTable = "application";
        public static final String applicationId = applicationTable + ".id_application";
        public static final String applicationApplication = applicationTable + ".application";
        public static final String applicationSearchTypeList = applicationTable + ".search_type_list";

        public static final String companyTable = "company";
        public static final String companyId = companyTable + ".id_company";
        public static final String companyCompany = companyTable + ".company";
        public static final String companyCompanyApplication = companyTable + ".company_application";

        public static final String travelTable = "travel";
        public static final String travelId = travelTable + ".id_travel";
        public static final String travelTravel = travelTable + ".travel";
        public static final String travelTravelCompany = travelTable + ".travel_company";

        public static final String travelDetailTable = "travel_detail";
        public static final String travelDetailId = travelDetailTable + ".id_travel_detail";
        public static final String travelDetailTravel = travelDetailTable + ".travel_detail_travel";
        public static final String travelDetailDayList = travelDetailTable + ".day_list";
        public static final String travelDetailStopList = travelDetailTable + ".stop_list";
        public static final String travelDetailTimeList = travelDetailTable + ".time_list";
        public static final String travelDetailSeasonList = travelDetailTable + ".season_list";
        public static final String travelDetailDayListDelimiter = ";";
        public static final String travelDetailStopListDelimiter = ";";
        public static final String travelDetailSeasonListDelimiter = ";";
        public static final String travelDetailTimeListDelimiter = "/";
        public static final String travelDetailTimeCommentListDelimiter = ";";
        public static final String travelDetailTimeInsideListDelimiter = ",";

        public static final String stopTable = "stop";
        public static final String stopId = stopTable + ".id_stop";
        public static final String stopStop = stopTable + ".stop";

        public static final String dayTable = "day";
        public static final String dayId = dayTable + ".id_day";
        public static final String dayDay = dayTable + ".day";

        public static final String seasonTable = "season";
        public static final String seasonId = seasonTable + ".id_season";
        public static final String seasonSeason = seasonTable + ".season";

        public static final String searchTypeTable = "search_type";
        public static final String searchTypeId = searchTypeTable + ".id_search_type";
        public static final String searchTypeSearchType = searchTypeTable + ".search_type";





        public static final String tablaEmpresa = "Empresa";
        public static final String empresaEmpresaNombre = "EmpresaNombre";
        public static final String empresaCodigoEmpresa = "CodigoEmpresa";

        public static final String tablaRecorrido = "Recorrido";
        public static final String recorridoCodigoRecorrido = "CodigoRecorrido";
        public static final String recorridoRecorridoNombre = "RecorridoNombre";
        public static final String recorridoCodEmpresa = "CodEmpresa";

        public static final String tablaParada = "Parada";
        public static final String paradaCodigoParada = "CodigoParada";
        public static final String paradaParadaNombre = "ParadaNombre";

        public static final String tablaRecorridoParada = "RecorridoParada";
        public static final String recorridoparadaCodigoRecoPara = "CodigoRecoPara";
        public static final String recorridoparadaCodRecorrido = "CodRecorrido";
        public static final String recorridoparadaCodParada = "CodParada";

        public static final String tablaFecha = "Fecha";
        public static final String fechaCodigoFecha = "CodigoFecha";
        public static final String fechaFechaNombre = "FechaNombre";

        public static final String tablaViaje = "Viaje";
        public static final String viajeCodigoViaje = "CodigoViaje";
        public static final String viajeCodRecorrido = "CodRecorrido";
        public static final String viajeCodFecha = "CodFecha";
        public static final String viajeComentarioAnteriorViaje = "ComentarioAnteriorViaje";
        public static final String viajeComentarioSiguienteViaje = "ComentarioSiguienteViaje";
        public static final String viajeSentidoViaje = "SentidoViaje";

        public static final String tablaHorario = "HorarioParada";
        public static final String horarioCodigoHorario = "CodigoHorarioParada";
        public static final String horarioHoraHorario = "HoraHorarioParada";
        public static final String horarioMinutoHorario = "MinutoHorarioParada";
        public static final String horarioOrdenHorario = "OrdenHorarioParada";
        public static final String horarioCodParada = "CodParada";
        public static final String horarioCodViaje = "CodHorarioViaje";
        public static final String horarioComentarioAnteriorParada = "ComentarioAnteriorParada";
        public static final String horarioComentarioSiguienteParada = "ComentarioSiguienteParada";
        public static final String horarioPasaPorParada = "PasaPorParada";

        public static final String tablaSalida = "HorarioSalida";
        public static final String salidaCodigoSalida = "CodigoHorarioSalida";
        public static final String salidaHoraSalida = "HoraHorarioSalida";
        public static final String salidaMinutoSalida = "MinutoHorarioSalida";
        public static final String salidaCodViaje = "CodSalidaViaje";


        public static final String tablaFavoritos = "Favoritos";
        public static final String favoritosCodigoFavorito = "CodigoFavorito";
        public static final String favoritosTipoFavorito = "FavoritoTipo";
        public static final String favoritosRecorrido = "FavoritoRecorrido";
        public static final String favoritosEmpresa = "FavoritoEmpresa";
        public static final String favoritosOrigen = "FavoritoOrigen";
        public static final String favoritosDestino = "FavoritoDestino";

        public static final String tablaUltimosUsados = "UltimosUsados";
        public static final String ultimosUsadosCodigoUltimosUsados = "CodigoUltimosUsados";
        public static final String ultimosUsadosTipoUltimosUsados = "UltimosUsadosTipo";
        public static final String ultimosUsadosRecorrido = "UltimosUsadosRecorrido";
        public static final String ultimosUsadosEmpresa = "UltimosUsadosEmpresa";
        public static final String ultimosUsadosParada = "UltimosUsadosParada";
        public static final String ultimosUsadosOrigen = "UltimosUsadosOrigen";
        public static final String ultimosUsadosDestino = "UltimosUsadosDestino";
        public static final String ultimosUsadosOrigenDestino = "UltimosUsadosOrigenDestino";
        public static final String ultimosUsadosOrden = "UltimosUsadosOrden";
    }

    public static class SearchTag {

        public static final String applicationId = "$application_id";
        public static final String companyId = "$company_id";
        public static final String travelId = "$company_id";

    }

    public static class Queries {

        public static final String QUERY_ALL_APPLICATIONS = "select " +
                Data.applicationId + " , " +
                Data.applicationApplication + " , " +
                Data.applicationSearchTypeList +
                " from " + Data.applicationTable;


        public static final String QUERY_COMPANY_LIST_BY_APPLICATION = "select " +
                Data.companyId + " , " +
                Data.companyCompany  +
                " from " + Data.companyTable +
                " inner join " + Data.applicationTable + " on " +
                Data.companyCompanyApplication + " = " +
                Data.applicationApplication+
                " where " +
                Data.applicationId + " = " +
                SearchTag.applicationId;

        public static final String QUERY_TRAVEL_LIST_BY_COMPANY = "select " +
                Data.travelId + " , " +
                Data.travelTravel  +
                " from " + Data.travelTable +
                " inner join " + Data.companyTable + " on " +
                Data.travelTravelCompany + " = " +
                Data.companyCompany +
                " where " +
                Data.companyId + " = " +
                SearchTag.companyId;


        public static final String QUERY_TRAVEL_DETAIL_LIST_BY_TRAVEL = "select " +
                Data.travelDetailId + " , " +
                Data.travelDetailStopList  + " , " +
                Data.travelDetailSeasonList  + " , " +
                Data.travelDetailDayList  + " , " +
                Data.travelDetailTimeList  +
                " from " + Data.travelDetailTable +
                " inner join " + Data.travelTable + " on " +
                Data.travelDetailTravel + " = " +
                Data.travelTravel +
                " where " +
                Data.travelId + " = " +
                SearchTag.travelId;

    }
}

package littlecrow.horarius.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import java.io.IOException;

public enum HorariusDataSource {

    INSTANCE;
    private SQLiteDatabase database;
    private boolean isDbClosed =true;
    private AdminSQLiteOpenHelper adminOpenHelper;
    


    public void init(Context context) throws SQLException, IOException {
        adminOpenHelper= new AdminSQLiteOpenHelper(context);
        if(isDbClosed){
            isDbClosed =false;

            adminOpenHelper.createDataBase();

            database = adminOpenHelper.getWritableDatabase();

        }

    }

    public boolean necessaryUpdate(){

        return adminOpenHelper.getRequiredUpdate();

    }

    public void updateDB(){

        closeDatabase();

        try {
            adminOpenHelper.createDataBase();
        }catch (IOException e) {
            e.printStackTrace();
        }

        database = adminOpenHelper.getWritableDatabase();

    }

    public boolean isDatabaseClosed(){
        return isDbClosed;
    }

    public void closeDatabase(){
        if(!isDbClosed && database != null){
            isDbClosed =true;
            database.close();
        }
    }

    public Cursor runRawQuery(String query){
        return database.rawQuery(query,null);
    }

    public Cursor getApplicationList(){
        return runRawQuery(DBConstants.Queries.QUERY_ALL_APPLICATIONS);
    }

    public Cursor getCompanyListByApplication(Integer applicationId){
        String query = DBConstants.Queries.QUERY_COMPANY_LIST_BY_APPLICATION;
        query = query.replace(DBConstants.SearchTag.applicationId, applicationId.toString());
        return runRawQuery(query);
    }


    public Cursor getTravelListByCompany(Integer companyId){
        String query = DBConstants.Queries.QUERY_TRAVEL_LIST_BY_COMPANY;
        query = query.replace(DBConstants.SearchTag.companyId, companyId.toString());
        return runRawQuery(query);
    }

    public Cursor getTravelDetailListByTravel(Integer travelId){
        String query = DBConstants.Queries.QUERY_TRAVEL_DETAIL_LIST_BY_TRAVEL;
        query = query.replace(DBConstants.SearchTag.travelId, travelId.toString());
        return runRawQuery(query);
    }














    public Cursor todasLasEmpresas(){

        return database.rawQuery(" select " + DBConstants.Data.empresaEmpresaNombre +
                " from " + DBConstants.Data.tablaEmpresa + " " +
                " order by " + DBConstants.Data.empresaEmpresaNombre, null);
    }

    public Cursor recorridosEmpresa(String empresa){

        return database.rawQuery("select " + DBConstants.Data.recorridoRecorridoNombre +
                " from " + DBConstants.Data.tablaRecorrido + "," +
                DBConstants.Data.tablaEmpresa +
                " where " + DBConstants.Data.recorridoCodEmpresa +
                " = " + DBConstants.Data.empresaCodigoEmpresa +
                " And " + DBConstants.Data.empresaEmpresaNombre +
                " = '" + empresa + "' " +
                "order by " + DBConstants.Data.recorridoRecorridoNombre,null);
    }

    public Cursor paradasRecorrido(String empresa,String recorrido){

        return database.rawQuery("select distinct  " + DBConstants.Data.paradaParadaNombre +
                " from " + DBConstants.Data.tablaParada + " , " +
                DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaRecorrido + " , " +
                DBConstants.Data.tablaHorario + " , " +
                DBConstants.Data.tablaEmpresa +
                " where " +
                DBConstants.Data.viajeCodigoViaje + " = " +
                DBConstants.Data.horarioCodViaje + " AND " +
                DBConstants.Data.horarioCodParada + " = " +
                DBConstants.Data.paradaCodigoParada + " AND " +
                DBConstants.Data.viajeSentidoViaje + " = 1 AND " +
                DBConstants.Data.recorridoRecorridoNombre + " = " +
                " '" + recorrido + "' " + " AND " +
                DBConstants.Data.recorridoCodigoRecorrido + " = " +
                DBConstants.Data.viajeCodRecorrido + " AND " +
                DBConstants.Data.recorridoCodEmpresa + " = " +
                DBConstants.Data.empresaCodigoEmpresa + " AND " +
                DBConstants.Data.empresaEmpresaNombre + " ='" +
                empresa + "' " +
                " order by " + DBConstants.Data.horarioOrdenHorario, null);
    }

    public Cursor paradasOrigen(){

        return database.rawQuery("select distinct " + DBConstants.Data.paradaParadaNombre +
                " from " + DBConstants.Data.tablaParada +
                " order by " + DBConstants.Data.paradaParadaNombre,null);
    }

    public Cursor paradasDestino(String origen){

        return database.rawQuery("select distinct " + DBConstants.Data.paradaParadaNombre +
                " from " + DBConstants.Data.tablaParada + "," +
                DBConstants.Data.tablaHorario + "," +
                DBConstants.Data.tablaViaje +
                " where " + DBConstants.Data.horarioCodParada + " = " +
                DBConstants.Data.paradaCodigoParada + " AND " +
                DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND " +
                DBConstants.Data.viajeCodigoViaje +
                " IN (select " + DBConstants.Data.viajeCodigoViaje +
                " from " + DBConstants.Data.tablaHorario + "," +
                DBConstants.Data.tablaParada + "," +
                DBConstants.Data.tablaViaje +
                " where " + DBConstants.Data.viajeCodigoViaje + " = " +
                DBConstants.Data.horarioCodViaje + " AND " +
                DBConstants.Data.horarioCodParada + " = " +
                DBConstants.Data.paradaCodigoParada + " AND " +
                DBConstants.Data.paradaParadaNombre + " = '" +
                origen + "' ) AND " +
                DBConstants.Data.paradaParadaNombre + " <> '" +
                origen + "' " +
                " order by " + DBConstants.Data.paradaParadaNombre,null);
    }

    public void agregarFavorito(String tipoBusqueda,String eleccionEmpresa, String eleccionRecorrido, String eleccionOrigen, String eleccionDestino){

        /*if(tipoBusqueda.equals(MainActivity.tipoBusquedaRecorrido)){

            database.execSQL("INSERT INTO " + DBConstants.Data.tablaFavoritos + "(" +
                    DBConstants.Data.favoritosTipoFavorito + "," +
                    DBConstants.Data.favoritosEmpresa + "," +
                    DBConstants.Data.favoritosRecorrido + ")" +
                    "VALUES('" + tipoBusqueda + "','" +
                    eleccionEmpresa + "' ,'" +
                    eleccionRecorrido + "')");

        }else{
            if(tipoBusqueda.equals(MainActivity.tipoBusquedaOrigenDestino)){

                database.execSQL("INSERT INTO " + DBConstants.Data.tablaFavoritos + "(" +
                        DBConstants.Data.favoritosTipoFavorito + "," +
                        DBConstants.Data.favoritosOrigen + " ," +
                        DBConstants.Data.favoritosDestino + ")" +
                        "VALUES('" + tipoBusqueda + "'," +
                        "'" +
                        eleccionOrigen + "' ,'" +
                        eleccionDestino + "')");

            }
        }
*/

    }

    public Cursor buscarTodosFavoritos(){
        return database.rawQuery("select distinct * " +
                " from " + DBConstants.Data.tablaFavoritos +
                " order by " + DBConstants.Data.favoritosCodigoFavorito,null);
    }

    public Cursor buscarDias(){
        return database.rawQuery("select distinct " + DBConstants.Data.fechaFechaNombre +
                " from " + DBConstants.Data.tablaFecha +
                " order by " + DBConstants.Data.fechaCodigoFecha,null);
    }

    public void eliminarFavorito(String tipoBusqueda,String eleccionEmpresa, String eleccionRecorrido, String eleccionOrigen, String eleccionDestino){
        Cursor encontrarFavorito;
        /*if(tipoBusqueda.equals(MainActivity.tipoBusquedaRecorrido)){
            encontrarFavorito=database.rawQuery("select * " +
                    " from " + DBConstants.Data.tablaFavoritos +
                    " where " + DBConstants.Data.favoritosTipoFavorito + " = '" +
                    tipoBusqueda + "' " + " AND " +
                    DBConstants.Data.favoritosEmpresa + " = " +
                    " '" + eleccionEmpresa + "' AND " +
                    DBConstants.Data.favoritosRecorrido + " = " +
                    " '" + eleccionRecorrido + "' ",null);
        }else{

            encontrarFavorito=database.rawQuery("select * " +
                    " from " + DBConstants.Data.tablaFavoritos +
                    " where " + DBConstants.Data.favoritosTipoFavorito + " = '" +
                    tipoBusqueda + "' " + " AND " +
                    DBConstants.Data.favoritosOrigen + " = " +
                    " '" + eleccionOrigen + "' AND " +
                    DBConstants.Data.favoritosDestino + " = " +
                    " '" + eleccionDestino + "' ",null);

        }

        while(encontrarFavorito.moveToNext()){
            Integer codigoFavoritoEliminar=encontrarFavorito.getInt(0);
            database.execSQL("delete from " + DBConstants.Data.tablaFavoritos +
                    " where " + DBConstants.Data.favoritosCodigoFavorito + " = " +
                    codigoFavoritoEliminar);

        }
*/

    }

    public boolean verificarFavorito(String tipoBusqueda,String eleccionEmpresa, String eleccionRecorrido, String eleccionOrigen, String eleccionDestino){

        Cursor verificacionFavorito;

        /*if(tipoBusqueda.equals(MainActivity.tipoBusquedaRecorrido)){
            verificacionFavorito=database.rawQuery("select * " +
                    " from " + DBConstants.Data.tablaFavoritos +
                    " where " + DBConstants.Data.favoritosTipoFavorito + " = '" +
                    tipoBusqueda + "' " + " AND " +
                    DBConstants.Data.favoritosEmpresa + " = " +
                    " '" + eleccionEmpresa + "' AND " +
                    DBConstants.Data.favoritosRecorrido + " = " +
                    " '" + eleccionRecorrido + "' ",null);
        }else{

            verificacionFavorito=database.rawQuery("select * " +
                    " from " + DBConstants.Data.tablaFavoritos +
                    " where " + DBConstants.Data.favoritosTipoFavorito + " = '" +
                    tipoBusqueda + "' " + " AND " +
                    DBConstants.Data.favoritosOrigen + " = " +
                    " '" + eleccionOrigen + "' AND " +
                    DBConstants.Data.favoritosDestino + " = " +
                    " '" + eleccionDestino + "' ",null);

        }

        if(verificacionFavorito.moveToNext()){
            return true;
        }else{
            return false;
        }*/

        return false;
    }

    public Cursor busquedaHorariosOrigenDestinoDestino(String origen, String destino, String fecha, Integer horaBuscadaMax, Integer horaBuscadaMin){


        Integer horaBuscadaMinima= horaBuscadaMin - 1;
        if(horaBuscadaMinima<3 && horaBuscadaMin>=3){
            horaBuscadaMinima = 3;
        }
        Integer horaBuscadaMaxima= horaBuscadaMax;

        //Log.i(this.getClass().toString()," select ");


        return database.rawQuery(" select " + DBConstants.Data.salidaHoraSalida + ","  +
                DBConstants.Data.salidaMinutoSalida + " , " +
                " HO." + DBConstants.Data.horarioHoraHorario + " , " +
                " HO." + DBConstants.Data.horarioMinutoHorario + " , " +
                " HD." + DBConstants.Data.horarioHoraHorario + " , " +
                " HD." + DBConstants.Data.horarioMinutoHorario + " , " +
                DBConstants.Data.recorridoRecorridoNombre + ", " +
                DBConstants.Data.empresaEmpresaNombre +  " , " +
                " HO." + DBConstants.Data.horarioComentarioAnteriorParada + " , " +
                " HO." + DBConstants.Data.horarioComentarioSiguienteParada +  " , " +
                " HD." + DBConstants.Data.horarioComentarioAnteriorParada + " , " +
                " HD." + DBConstants.Data.horarioComentarioSiguienteParada + " , " +
                DBConstants.Data.viajeComentarioAnteriorViaje + " , " +
                DBConstants.Data.viajeComentarioSiguienteViaje +
                " from " + DBConstants.Data.tablaSalida + " , " +
                DBConstants.Data.tablaHorario + " as HO ," +
                DBConstants.Data.tablaHorario + " as HD ," +
                DBConstants.Data.tablaParada + " as PO, " +
                DBConstants.Data.tablaParada + " as PD, " +
                DBConstants.Data.tablaViaje + ", " +
                DBConstants.Data.tablaFecha + ", " +
                DBConstants.Data.tablaRecorrido + ", " +
                DBConstants.Data.tablaEmpresa +
                " where " + DBConstants.Data.salidaCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND " +
                " HO." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HD." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HO." + DBConstants.Data.horarioCodParada + " = " +
                " PO." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioCodParada + " = " +
                " PD." + DBConstants.Data.paradaCodigoParada + " AND " +
                " PO." + DBConstants.Data.paradaParadaNombre + " = '" + origen + "' AND " +
                " PD." + DBConstants.Data.paradaParadaNombre + " = '" + destino + "' AND " +
                DBConstants.Data.viajeCodFecha + " = " +
                DBConstants.Data.fechaCodigoFecha + " AND " +
                DBConstants.Data.fechaFechaNombre + " = " +
                " '" + fecha + "' AND " +
                DBConstants.Data.viajeCodRecorrido + " = " +
                DBConstants.Data.recorridoCodigoRecorrido + " AND " +
                DBConstants.Data.recorridoCodEmpresa + " = " +
                DBConstants.Data.empresaCodigoEmpresa + " AND (" +
                DBConstants.Data.salidaHoraSalida + " + " +
                " HD." + DBConstants.Data.horarioHoraHorario + " ) between " +
                horaBuscadaMinima + " AND " + horaBuscadaMaxima + " AND " +
                DBConstants.Data.viajeCodigoViaje + " IN (" +
                "select " + DBConstants.Data.viajeCodigoViaje +
                " from " + DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaHorario + " as HO ," +
                DBConstants.Data.tablaHorario + " as HD ," +
                DBConstants.Data.tablaParada + " as PO, " +
                DBConstants.Data.tablaParada + " as PD " +
                " where " + " HO." + DBConstants.Data.horarioOrdenHorario + " < " +
                " HD." + DBConstants.Data.horarioOrdenHorario + " AND " +
                " HO." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HD." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HO." + DBConstants.Data.horarioCodParada + " = " +
                " PO." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioCodParada + " = " +
                " PD." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioPasaPorParada + " = 0 AND " +
                " HO." + DBConstants.Data.horarioPasaPorParada + " = 0 AND " +
                " PO." + DBConstants.Data.paradaParadaNombre + " = '" + origen + "' AND " +
                " PD." + DBConstants.Data.paradaParadaNombre + " = '" + destino + "')" +
                " order by " + DBConstants.Data.empresaEmpresaNombre + " , " +
                DBConstants.Data.recorridoRecorridoNombre + ", " +
                DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida,null);
    }

    public Cursor busquedaHorariosOrigenDestinoOrigen(String origen, String destino, String fecha, Integer horaBuscadaMax, Integer horaBuscadaMin){

        Integer horaBuscadaMinima= horaBuscadaMin - 1;
        if(horaBuscadaMinima<3 && horaBuscadaMin>=3){
            horaBuscadaMinima = 3;
        }
        Integer horaBuscadaMaxima= horaBuscadaMax;

        //Log.i(this.getClass().toString(), " select " );


        return database.rawQuery(" select " + DBConstants.Data.salidaHoraSalida + ","  +
                DBConstants.Data.salidaMinutoSalida + " , " +
                " HO." + DBConstants.Data.horarioHoraHorario + " , " +
                " HO." + DBConstants.Data.horarioMinutoHorario + " , " +
                " HD." + DBConstants.Data.horarioHoraHorario + " , " +
                " HD." + DBConstants.Data.horarioMinutoHorario + " , " +
                DBConstants.Data.recorridoRecorridoNombre + ", " +
                DBConstants.Data.empresaEmpresaNombre +  " , " +
                " HO." + DBConstants.Data.horarioComentarioAnteriorParada + " , " +
                " HO." + DBConstants.Data.horarioComentarioSiguienteParada +  " , " +
                " HD." + DBConstants.Data.horarioComentarioAnteriorParada + " , " +
                " HD." + DBConstants.Data.horarioComentarioSiguienteParada + " , " +
                DBConstants.Data.viajeComentarioAnteriorViaje + " , " +
                DBConstants.Data.viajeComentarioSiguienteViaje +
                " from " + DBConstants.Data.tablaSalida + " , " +
                DBConstants.Data.tablaHorario + " as HO ," +
                DBConstants.Data.tablaHorario + " as HD ," +
                DBConstants.Data.tablaParada + " as PO, " +
                DBConstants.Data.tablaParada + " as PD, " +
                DBConstants.Data.tablaViaje + ", " +
                DBConstants.Data.tablaFecha + ", " +
                DBConstants.Data.tablaRecorrido + ", " +
                DBConstants.Data.tablaEmpresa +
                " where " + DBConstants.Data.salidaCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND " +
                " HO." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HD." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HO." + DBConstants.Data.horarioCodParada + " = " +
                " PO." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioCodParada + " = " +
                " PD." + DBConstants.Data.paradaCodigoParada + " AND " +
                " PO." + DBConstants.Data.paradaParadaNombre + " = '" + origen + "' AND " +
                " PD." + DBConstants.Data.paradaParadaNombre + " = '" + destino + "' AND " +
                DBConstants.Data.viajeCodFecha + " = " +
                DBConstants.Data.fechaCodigoFecha + " AND " +
                DBConstants.Data.fechaFechaNombre + " = " +
                " '" + fecha + "' AND " +
                DBConstants.Data.viajeCodRecorrido + " = " +
                DBConstants.Data.recorridoCodigoRecorrido + " AND " +
                DBConstants.Data.recorridoCodEmpresa + " = " +
                DBConstants.Data.empresaCodigoEmpresa + " AND (" +
                DBConstants.Data.salidaHoraSalida + " + " +
                " HO." + DBConstants.Data.horarioHoraHorario + " ) between " +
                horaBuscadaMinima + " AND " + horaBuscadaMaxima + " AND " +
                DBConstants.Data.viajeCodigoViaje + " IN (" +
                "select " + DBConstants.Data.viajeCodigoViaje +
                " from " + DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaHorario + " as HO ," +
                DBConstants.Data.tablaHorario + " as HD ," +
                DBConstants.Data.tablaParada + " as PO, " +
                DBConstants.Data.tablaParada + " as PD " +
                " where " + " HO." + DBConstants.Data.horarioOrdenHorario + " < " +
                " HD." + DBConstants.Data.horarioOrdenHorario + " AND " +
                " HO." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HD." + DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND" +
                " HO." + DBConstants.Data.horarioCodParada + " = " +
                " PO." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioCodParada + " = " +
                " PD." + DBConstants.Data.paradaCodigoParada + " AND " +
                " HD." + DBConstants.Data.horarioPasaPorParada + " = 0 AND " +
                " HO." + DBConstants.Data.horarioPasaPorParada + " = 0 AND " +
                " PO." + DBConstants.Data.paradaParadaNombre + " = '" + origen + "' AND " +
                " PD." + DBConstants.Data.paradaParadaNombre + " = '" + destino + "')" +
                " order by " + DBConstants.Data.empresaEmpresaNombre + " , " +
                DBConstants.Data.recorridoRecorridoNombre + ", " +
                DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida,null);
    }

    public Cursor busquedaHorariosSalidaRecorridoTotal(String empresa, String recorrido, String fecha){

        return database.rawQuery(" select " + DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida + " , " +
                DBConstants.Data.viajeCodigoViaje + " , " +
                DBConstants.Data.viajeComentarioAnteriorViaje + " , " +
                DBConstants.Data.viajeComentarioSiguienteViaje + " , " +
                DBConstants.Data.viajeSentidoViaje +
                " from " + DBConstants.Data.tablaSalida + " , " +
                DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaFecha + " , " +
                DBConstants.Data.tablaEmpresa + " , " +
                DBConstants.Data.tablaRecorrido +
                " where " + DBConstants.Data.empresaEmpresaNombre + " = " +
                " '" + empresa + "' AND " +
                DBConstants.Data.empresaCodigoEmpresa + " = " +
                DBConstants.Data.recorridoCodEmpresa + " AND " +
                DBConstants.Data.recorridoRecorridoNombre + " = " +
                " '" + recorrido + "' AND " +
                DBConstants.Data.recorridoCodigoRecorrido + " = " +
                DBConstants.Data.viajeCodRecorrido + " AND " +
                DBConstants.Data.fechaFechaNombre + " = " +
                " '" + fecha + "' AND " +
                DBConstants.Data.fechaCodigoFecha + " = " +
                DBConstants.Data.viajeCodFecha + " AND " +
                DBConstants.Data.salidaCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje +
                " order by " + DBConstants.Data.viajeSentidoViaje + " , " +
                DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida,null);

    }

    public Cursor busquedaHorariosSalidaRecorrido(String empresa, String recorrido, String fecha, Integer horaBuscadaMin, Integer horaBuscadaMax){

        Integer horaBuscadaMinima= horaBuscadaMin - 4;
        if(horaBuscadaMinima<3 && horaBuscadaMin>=3){
            horaBuscadaMinima = 3;
        }
        Integer horaBuscadaMaxima= horaBuscadaMax;

        //Log.i(this.getClass().toString(), " select " );

        return database.rawQuery(" select " + DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida + " , " +
                DBConstants.Data.viajeCodigoViaje + " , " +
                DBConstants.Data.viajeComentarioAnteriorViaje + " , " +
                DBConstants.Data.viajeComentarioSiguienteViaje + " , " +
                DBConstants.Data.viajeSentidoViaje +
                " from " + DBConstants.Data.tablaSalida + " , " +
                DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaFecha + " , " +
                DBConstants.Data.tablaEmpresa + " , " +
                DBConstants.Data.tablaRecorrido +
                " where " + DBConstants.Data.empresaEmpresaNombre + " = " +
                " '" + empresa + "' AND " +
                DBConstants.Data.empresaCodigoEmpresa + " = " +
                DBConstants.Data.recorridoCodEmpresa + " AND " +
                DBConstants.Data.recorridoRecorridoNombre + " = " +
                " '" + recorrido + "' AND " +
                DBConstants.Data.recorridoCodigoRecorrido + " = " +
                DBConstants.Data.viajeCodRecorrido + " AND " +
                DBConstants.Data.fechaFechaNombre + " = " +
                " '" + fecha + "' AND " +
                DBConstants.Data.fechaCodigoFecha + " = " +
                DBConstants.Data.viajeCodFecha + " AND " +
                DBConstants.Data.salidaCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND " +
                DBConstants.Data.salidaCodigoSalida + " IN( " +
                "Select " + DBConstants.Data.salidaCodigoSalida +
                " from " + DBConstants.Data.tablaSalida +
                " where " + DBConstants.Data.salidaHoraSalida + " between " +
                horaBuscadaMinima + " AND " +
                horaBuscadaMaxima + " ) " +
                " order by " + DBConstants.Data.viajeSentidoViaje + " , " +
                DBConstants.Data.salidaHoraSalida + " , " +
                DBConstants.Data.salidaMinutoSalida,null);

    }

    public Cursor busquedaHorariosParadaRecorrido(String empresa, String recorrido, String fecha){
        return database.rawQuery(" select " + DBConstants.Data.horarioHoraHorario + " , " +
                DBConstants.Data.horarioMinutoHorario + " , " +
                DBConstants.Data.paradaParadaNombre + ", " +
                DBConstants.Data.horarioOrdenHorario + " , " +
                DBConstants.Data.viajeCodigoViaje + " , " +
                DBConstants.Data.horarioComentarioAnteriorParada + " , " +
                DBConstants.Data.horarioComentarioSiguienteParada + " , " +
                DBConstants.Data.viajeSentidoViaje + " , " +
                DBConstants.Data.horarioPasaPorParada +
                " from " + DBConstants.Data.tablaHorario  + " , " +
                DBConstants.Data.tablaViaje + " , " +
                DBConstants.Data.tablaParada + " , " +
                DBConstants.Data.tablaFecha + " , " +
                DBConstants.Data.tablaEmpresa + " , " +
                DBConstants.Data.tablaRecorrido +
                " where " + DBConstants.Data.empresaEmpresaNombre + " = " +
                " '" + empresa + "' AND " +
                DBConstants.Data.empresaCodigoEmpresa + " = " +
                DBConstants.Data.recorridoCodEmpresa + " AND " +
                DBConstants.Data.recorridoRecorridoNombre + " = " +
                " '" + recorrido + "' AND " +
                DBConstants.Data.recorridoCodigoRecorrido + " = " +
                DBConstants.Data.viajeCodRecorrido + " AND " +
                DBConstants.Data.fechaFechaNombre + " = " +
                " '" + fecha + "' AND " +
                DBConstants.Data.fechaCodigoFecha + " = " +
                DBConstants.Data.viajeCodFecha + " AND " +
                DBConstants.Data.horarioCodViaje + " = " +
                DBConstants.Data.viajeCodigoViaje + " AND " +
                DBConstants.Data.horarioCodParada + " = " +
                DBConstants.Data.paradaCodigoParada +
                " order by " + DBConstants.Data.viajeSentidoViaje + " , " +
                DBConstants.Data.viajeCodigoViaje + " , " +
                DBConstants.Data.horarioOrdenHorario,null);

    }

    public Cursor busquedaUltimosUsados(){

        return database.rawQuery("select distinct * " +
                " from " + DBConstants.Data.tablaUltimosUsados +
                " order by " + DBConstants.Data.ultimosUsadosOrden,null);

    }

    public void guardarUltimoUsado( String tipoBusqueda){

        /*Cursor verificacionUltimos;
        if(tipoBusqueda.equals(MainActivity.tipoBusquedaRecorrido)) {
            verificacionUltimos = database.rawQuery(" select " + DBConstants.Data.ultimosUsadosOrden +
                    " from " + DBConstants.Data.tablaUltimosUsados +
                    " where " + DBConstants.Data.ultimosUsadosEmpresa + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionEmpresa() + "' AND " +
                    DBConstants.Data.ultimosUsadosRecorrido + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionRecorrido() + "' AND " +
                    DBConstants.Data.ultimosUsadosParada + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionPasaPorRecorrido() + "'", null);
        }else{
            verificacionUltimos = database.rawQuery("select " + DBConstants.Data.ultimosUsadosOrden +
                    " from " + DBConstants.Data.tablaUltimosUsados +
                    " where " + DBConstants.Data.ultimosUsadosOrigen + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionOrigen() + "' AND " +
                    DBConstants.Data.ultimosUsadosDestino + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionDestino() + "' AND " +
                    DBConstants.Data.ultimosUsadosOrigenDestino + " = '" +
                    DTODatosBusqueda.getInstance().getEleccionPasaPorOrigenDestino() + "'", null);

        }

        int ordenUltimoExistente = 0;
        while(verificacionUltimos.moveToNext()){
            ordenUltimoExistente = verificacionUltimos.getInt(0);
        }
        if(ordenUltimoExistente == 0 || ordenUltimoExistente == 5){
            database.execSQL("DELETE FROM " + DBConstants.Data.tablaUltimosUsados +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 5 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 5 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 4 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 4 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 3 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 3 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 2 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 2 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 1 ");
        }
        if(ordenUltimoExistente==4) {
            database.execSQL("DELETE FROM " + DBConstants.Data.tablaUltimosUsados +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 4 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 4 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 3 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 3 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 2 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 2 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 1 ");
        }
        if(ordenUltimoExistente==3) {
            database.execSQL("DELETE FROM " + DBConstants.Data.tablaUltimosUsados +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 3 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 3 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 2 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 2 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 1 ");
        }
        if(ordenUltimoExistente==2){
            database.execSQL("DELETE FROM " + DBConstants.Data.tablaUltimosUsados +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 2 ");
            database.execSQL("UPDATE " + DBConstants.Data.tablaUltimosUsados +
                    " SET " + DBConstants.Data.ultimosUsadosOrden + " = 2 " +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 1 ");
        }
        if(ordenUltimoExistente==1){
            database.execSQL("DELETE FROM " + DBConstants.Data.tablaUltimosUsados +
                    " WHERE " + DBConstants.Data.ultimosUsadosOrden + " = 1 ");
        }

        if(tipoBusqueda.equals(MainActivity.tipoBusquedaRecorrido)){

            database.execSQL("INSERT INTO " + DBConstants.Data.tablaUltimosUsados + "(" +
                    DBConstants.Data.ultimosUsadosTipoUltimosUsados + "," +
                    DBConstants.Data.ultimosUsadosEmpresa + "," +
                    DBConstants.Data.ultimosUsadosRecorrido + "," +
                    DBConstants.Data.ultimosUsadosParada + "," +
                    DBConstants.Data.ultimosUsadosOrden + ")" +
                    "VALUES('" + tipoBusqueda + "','" +
                    DTODatosBusqueda.getInstance().getEleccionEmpresa() + "','" +
                    DTODatosBusqueda.getInstance().getEleccionRecorrido() + "' ,'" +
                    DTODatosBusqueda.getInstance().getEleccionPasaPorRecorrido() + "', 1 )");

        }else{
            if(tipoBusqueda.equals(MainActivity.tipoBusquedaOrigenDestino)){

                database.execSQL("INSERT INTO " + DBConstants.Data.tablaUltimosUsados + "(" +
                        DBConstants.Data.ultimosUsadosTipoUltimosUsados + "," +
                        DBConstants.Data.ultimosUsadosOrigen + " ," +
                        DBConstants.Data.ultimosUsadosDestino + "," +
                        DBConstants.Data.ultimosUsadosOrigenDestino + " , " +
                        DBConstants.Data.ultimosUsadosOrden + ")" +
                        "VALUES('" + tipoBusqueda + "'," +
                        "'" + DTODatosBusqueda.getInstance().getEleccionOrigen() + "' ,'" +
                        DTODatosBusqueda.getInstance().getEleccionDestino() + "' , '" +
                        DTODatosBusqueda.getInstance().getEleccionPasaPorOrigenDestino() + "' , 1)");

            }
        }*/

    }


    
}

package littlecrow.horarius.dayManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;

import java.util.Calendar;

import littlecrow.horarius.datasource.HorariusDataSource;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.DayData;
import littlecrow.horarius.timeMagament.AlertHourSelector;

public class AlertDaySelector extends DialogFragment {

    private DatePicker dialogChangeDayDatePicker;

    private String currentDay;

    private Button dialogChangeDayButtonAccept;
    private Button dialogChangeDayButtonCancel;
    private RadioButton dialogChangeDayRadioButtonWeek;
    private RadioButton dialogChangeDayRadioButtonSaturday;
    private RadioButton dialogChangeDayRadioButtonSunday;
    private RadioButton dialogChangeDayRadioButtonHolidays;
    private RadioButton dialogChangeDayRadioButtonSelect;

    ListenerDialogChangeDay listener;


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return createDialog();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        // Tus acciones
    }


    public AlertDialog createDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_change_day, null);

        builder.setView(v);


        dialogChangeDayDatePicker = v.findViewById(R.id.dialog_change_day_datepicker);

        dialogChangeDayRadioButtonWeek = v.findViewById(R.id.dialog_change_day_radiobutton_week);
        dialogChangeDayRadioButtonSaturday = v.findViewById(R.id.dialog_change_day_radiobutton_saturday);
        dialogChangeDayRadioButtonSunday = v.findViewById(R.id.dialog_change_day_radiobutton_sunday);
        dialogChangeDayRadioButtonHolidays = v.findViewById(R.id.dialog_change_day_radiobutton_holidays);
        dialogChangeDayRadioButtonSelect = v.findViewById(R.id.dialog_change_day_radiobutton_select);

        dialogChangeDayDatePicker.setEnabled(false);

        dialogChangeDayButtonAccept = (Button) v.findViewById(R.id.dialog_change_day_button_accept);
        dialogChangeDayButtonCancel = (Button) v.findViewById(R.id.dialog_change_day_button_cancel);

        currentDay = AccessData.getSearchData().getDayData().getDayData();

        if(currentDay.equals(DayUtils.DAY_WEEK)){
            dialogChangeDayRadioButtonWeek.setChecked(true);
        } else if(currentDay.equals(DayUtils.DAY_SATURDAY)){
            dialogChangeDayRadioButtonSaturday.setChecked(true);
        } else if(currentDay.equals(DayUtils.DAY_SUNDAY)){
            dialogChangeDayRadioButtonSunday.setChecked(true);
        } else if(currentDay.equals(DayUtils.DAY_HOLIDAYS)){
            dialogChangeDayRadioButtonHolidays.setChecked(true);
        }

        dialogChangeDayButtonAccept.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Crear Cuenta...
                        //dismiss();
                        DayData dayData = null;
                        if (dialogChangeDayRadioButtonSelect.isChecked()) {

                            Integer daySelected = dialogChangeDayDatePicker.getDayOfMonth();
                            Integer monthSelected = dialogChangeDayDatePicker.getMonth();
                            Integer yearSelected = dialogChangeDayDatePicker.getYear();

                            dayData = new DayData(daySelected,monthSelected,yearSelected);


                        } else if (dialogChangeDayRadioButtonWeek.isChecked()){
                            dayData = new DayData(DayUtils.DAY_WEEK);
                        } else if (dialogChangeDayRadioButtonSaturday.isChecked()){
                            dayData = new DayData(DayUtils.DAY_SATURDAY);
                        } else if (dialogChangeDayRadioButtonSunday.isChecked()){
                            dayData = new DayData(DayUtils.DAY_SUNDAY);
                        } else if (dialogChangeDayRadioButtonHolidays.isChecked()){
                            dayData = new DayData(DayUtils.DAY_HOLIDAYS);
                        }

                        AccessData.getSearchData().setDayData(dayData);


                        listener.acceptDayChangeAction();

                        dismiss();

                    }
                }
        );

        dialogChangeDayButtonCancel.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        listener.cancelDayChangeAction();

                        dismiss();
                    }
                }

        );
        dialogChangeDayRadioButtonWeek.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogChangeDayDatePicker.setEnabled(false);
                    }
                }
        );
        dialogChangeDayRadioButtonSaturday.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogChangeDayDatePicker.setEnabled(false);
                    }
                }
        );
        dialogChangeDayRadioButtonSunday.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogChangeDayDatePicker.setEnabled(false);
                    }
                }
        );
        dialogChangeDayRadioButtonHolidays.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogChangeDayDatePicker.setEnabled(false);
                    }
                }
        );
            /*botonDiaParo.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialogChangeDayDatePicker.setEnabled(false);
                            fechaSeleccionada=textoDiaParo;
                        }
                    }
            );*/
        dialogChangeDayRadioButtonSelect.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogChangeDayDatePicker.setEnabled(true);

                    }
                }
        );

        return builder.create();
    }

    public interface ListenerDialogChangeDay {

        void acceptDayChangeAction();// Eventos Botón Positivo
        void cancelDayChangeAction();// Eventos Botón Negativo
    }

    public void attachFragment(Fragment fragment) {
        try {
            listener = (AlertDaySelector.ListenerDialogChangeDay) fragment;

        } catch (ClassCastException e) {
            throw new ClassCastException(
                    fragment.toString() +
                            " no implementó OnSimpleDialogListener");

        }
    }


}

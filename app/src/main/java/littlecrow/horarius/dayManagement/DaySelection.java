package littlecrow.horarius.dayManagement;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import littlecrow.horarius.activities.selection.MainActivity;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.DayData;

public class DaySelection extends Fragment implements AlertDaySelector.ListenerDialogChangeDay  {


    private LinearLayout selectionDayLinearLayoutSelected;
    private TextView selectionDayTextDaySelected;
    private TextView selectionDayTextDateSelected;

    private AlertDaySelector dialogChangeDay;


    public DaySelection() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.selection_day, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {

        selectionDayLinearLayoutSelected = getView().findViewById(R.id.selection_day_linearlayout_day_selected);
        selectionDayLinearLayoutSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDay(v);
            }
        });
        selectionDayTextDaySelected = getView().findViewById(R.id.selection_day_text_day_selected);
        selectionDayTextDateSelected = getView().findViewById(R.id.selection_day_text_date_selected);

        setDayText();

    }

    public void changeDay(View view){

        dialogChangeDay = new AlertDaySelector();
        dialogChangeDay.attachFragment(this);
        dialogChangeDay.show(getFragmentManager(), "AlertDaySelector");
    }

    public void acceptDayChangeAction(){

        setDayText();

    }

    public void cancelDayChangeAction(){

    }

    private void setDayText(){
        DayData dayData = AccessData.getSearchData().getDayData();

        selectionDayTextDaySelected.setText(dayData.getDayData());
        if(null == dayData.getDayNumber()){
            selectionDayTextDateSelected.setVisibility(View.GONE);
        }else{
            selectionDayTextDateSelected.setText(dayData.getDayNumber());
            selectionDayTextDateSelected.setVisibility(View.VISIBLE);
        }

        MainActivity.buttonSearchEnabler();
    }
}


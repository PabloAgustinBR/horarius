package littlecrow.horarius.dayManagement;

import android.content.Context;

import java.util.Calendar;

import littlecrow.horarius.App;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.DayData;


public class DayUtils {

    public static final String DAY_WEEK_KEY = "week";
    public static final String DAY_WEEK = App.getResourses().getString(R.string.selection_day_text_week);
    public static final String DAY_SATURDAY_KEY = "saturday";
    public static final String DAY_SATURDAY = App.getResourses().getString(R.string.selection_day_text_saturday);
    public static final String DAY_SUNDAY_KEY = "sunday";
    public static final String DAY_SUNDAY = App.getResourses().getString(R.string.selection_day_text_sunday);
    public static final String DAY_HOLIDAYS_KEY = "holidays";
    public static final String DAY_HOLIDAYS = App.getResourses().getString(R.string.selection_day_text_holidays);


    public static void setInitialDay() {

        Calendar calendar = Calendar.getInstance();
        fixCalendarDay(calendar);
        DayData dayData = new DayData(calendar);
        AccessData.getSearchData().setDayData(dayData);
    }

    public static String getDayKey(Calendar calendar) {

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                return DAY_WEEK_KEY;
            case Calendar.TUESDAY:
                return DAY_WEEK_KEY;
            case Calendar.WEDNESDAY:
                return DAY_WEEK_KEY;
            case Calendar.THURSDAY:
                return DAY_WEEK_KEY;
            case Calendar.FRIDAY:
                return DAY_WEEK_KEY;
            case Calendar.SATURDAY:
                return DAY_SATURDAY_KEY;
            case Calendar.SUNDAY:
                return DAY_SUNDAY_KEY;
            default:
                return null;
        }
    }


    public static String getDay(String dayKey) {

        switch (dayKey) {
            case DAY_WEEK_KEY:
                return DAY_WEEK;
            case DAY_SATURDAY_KEY:
                return DAY_SATURDAY;
            case DAY_SUNDAY_KEY:
                return DAY_SUNDAY;
            case DAY_HOLIDAYS_KEY:
                return DAY_HOLIDAYS;
            default:
                return null;
        }
    }

    public static void fixCalendarDay(Calendar calendar){
        if(calendar.get(Calendar.HOUR_OF_DAY) <= 3){
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
    }
}

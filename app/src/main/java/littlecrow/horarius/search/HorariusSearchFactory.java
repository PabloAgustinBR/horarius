package littlecrow.horarius.search;

import littlecrow.horarius.App;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.search.impl.HorariusSearchByCompany;
import littlecrow.horarius.search.impl.HorariusSearchByStops;

public enum HorariusSearchFactory {

    INSTANCE;

    private static final String SEARCH_TYPE_BY_COMPANY = "company";
    private static final String SEARCH_TYPE_BY_STOPS = "stops";

    public HorariusSearch getHorariusSearch(){
        switch (AccessData.getSearchData().getSearchType()){
            case SEARCH_TYPE_BY_COMPANY:
                return new HorariusSearchByCompany();
            case SEARCH_TYPE_BY_STOPS:
                return new HorariusSearchByStops();
            default:
                return null;
        }
    }
}

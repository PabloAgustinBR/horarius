package littlecrow.horarius.search.impl;

import java.util.ArrayList;
import java.util.List;

import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.applicationData.CompanyData;
import littlecrow.horarius.data.applicationData.TravelData;
import littlecrow.horarius.data.applicationData.TravelDetailData;
import littlecrow.horarius.data.applicationData.TravelDetailTimeData;
import littlecrow.horarius.data.searchData.DayData;
import littlecrow.horarius.data.searchData.ResultData;
import littlecrow.horarius.data.searchData.TimeData;
import littlecrow.horarius.search.HorariusSearch;

public class HorariusSearchByCompany implements HorariusSearch {
    @Override
    public void horariusSearch() {
        AccessData.setResultDataList(new ArrayList<ResultData>());

        ApplicationData applicationData = AccessData.getApplicationData(AccessData.getSearchData().getSearchApplication());
        CompanyData companyData = applicationData.getCompanyData(AccessData.getSearchData().getSearchCompany());
        TravelData travelData = companyData.getTravelData(AccessData.getSearchData().getSearchTravel());
        String stop = AccessData.getSearchData().getSearchStop();
        DayData dayData = AccessData.getSearchData().getDayData();
        TimeData timeDataBegin = AccessData.getSearchData().getTimeBegin();
        TimeData timeDataEnd = AccessData.getSearchData().getTimeEnd();

        for(TravelDetailData travelDetailData : travelData.getTravelDetailDataList()){
            List<String> stopList = travelDetailData.getStopList();
            if(stopList.contains(stop) && travelDetailData.getDayList().contains(dayData.getDayKey())){
                List<Integer> stopIndexList = new ArrayList<>();
                for (int i = 0; i < stopList.size(); i++) {
                    if(stop.equals(stopList.get(i))){
                        stopIndexList.add(i);
                    }
                }
                for(TravelDetailTimeData travelDetailTimeData : travelDetailData.getTravelDetailTimeDataList()){
                    boolean selected = false;
                    for(Integer stopIndex : stopIndexList){
                        TimeData timeData = travelDetailTimeData.getTimeList().get(stopIndex);
                        if(timeData.isTimeDataAfterOrEquals(timeDataBegin) && timeData.isTimeDataBeforeOrEquals(timeDataEnd)){
                            selected = true;
                            break;
                        }
                    }
                    if(selected){
                        ResultData resultData = new ResultData();
                        resultData.setApplicationData(applicationData);
                        resultData.setCompanyData(companyData);
                        resultData.setTravelData(travelData);
                        resultData.setTravelDetailData(travelDetailData);
                        resultData.addTravelDetailTimeData(travelDetailTimeData);
                        AccessData.addResultData(resultData);
                    }
                }

            }
        }
    }
}

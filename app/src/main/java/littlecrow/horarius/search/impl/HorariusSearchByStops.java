package littlecrow.horarius.search.impl;

import java.util.ArrayList;
import java.util.List;

import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.applicationData.ApplicationData;
import littlecrow.horarius.data.applicationData.CompanyData;
import littlecrow.horarius.data.applicationData.TravelData;
import littlecrow.horarius.data.applicationData.TravelDetailData;
import littlecrow.horarius.data.applicationData.TravelDetailTimeData;
import littlecrow.horarius.data.searchData.DayData;
import littlecrow.horarius.data.searchData.ResultData;
import littlecrow.horarius.data.searchData.TimeData;
import littlecrow.horarius.search.HorariusSearch;

public class HorariusSearchByStops implements HorariusSearch {

    @Override
    public void horariusSearch() {
        AccessData.setResultDataList(new ArrayList<ResultData>());

        ApplicationData applicationData = AccessData.getApplicationData(AccessData.getSearchData().getSearchApplication());
        String stopOrigin = AccessData.getSearchData().getSearchOrigin();
        String stopDestiny = AccessData.getSearchData().getSearchDestiny();
        DayData dayData = AccessData.getSearchData().getDayData();
        TimeData timeDataBegin = AccessData.getSearchData().getTimeBegin();
        TimeData timeDataEnd = AccessData.getSearchData().getTimeEnd();
        for(CompanyData companyData : applicationData.getCompanyDataMap().values()){
            for(TravelData travelData : companyData.getTravelDataMap().values()){
                for(TravelDetailData travelDetailData : travelData.getTravelDetailDataList()){
                    List<String> stopList = travelDetailData.getStopList();
                    if(travelDetailData.getDayList().contains(dayData.getDayKey()) && stopList.contains(stopOrigin) && stopList.contains(stopDestiny) && (stopList.indexOf(stopOrigin) < stopList.indexOf(stopDestiny))){
                        List<Integer> stopIndexList = new ArrayList<>();
                        for (int i = 0; i < stopList.size(); i++) {
                            if(stopOrigin.equals(stopList.get(i)) || stopDestiny.equals(stopList.get(i))){
                                stopIndexList.add(i);
                            }
                        }
                        for(TravelDetailTimeData travelDetailTimeData : travelDetailData.getTravelDetailTimeDataList()){
                            boolean selected = false;
                            for(Integer stopIndex : stopIndexList){
                                TimeData timeData = travelDetailTimeData.getTimeList().get(stopIndex);
                                if(timeData.isTimeDataAfterOrEquals(timeDataBegin) && timeData.isTimeDataBeforeOrEquals(timeDataEnd)){
                                    selected = true;
                                    break;
                                }
                            }
                            if(selected){
                                ResultData resultData = new ResultData();
                                resultData.setApplicationData(applicationData);
                                resultData.setCompanyData(companyData);
                                resultData.setTravelData(travelData);
                                resultData.setTravelDetailData(travelDetailData);
                                resultData.addTravelDetailTimeData(travelDetailTimeData);
                                AccessData.addResultData(resultData);
                            }
                        }

                    }
                }
            }
        }

    }
}

package littlecrow.horarius.timeMagament;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.TimeData;

public class AlertHourSelector extends DialogFragment {

    public static final Integer TIME_BEGIN = 1;
    public static final Integer TIME_END = 2;

    private Integer timeType;

    private TimePicker dialogChangeHourTimePicker;

    private Button dialogChangeHourButtonAccept;
    private Button dialogChangeHourButtonCancel;

    ListenerDialogChangeHour listener;


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        // Tus acciones
    }


    public AlertDialog createDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_change_hour, null);

        builder.setView(v);

        dialogChangeHourTimePicker = v.findViewById(R.id.dialog_change_hour_timepicker);
        dialogChangeHourTimePicker.setIs24HourView(true);

        dialogChangeHourButtonAccept = v.findViewById(R.id.dialog_change_hour_button_accept);
        dialogChangeHourButtonCancel = v.findViewById(R.id.dialog_change_hour_button_cancel);



        dialogChangeHourButtonAccept.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //dismiss();
                        Integer chosenHour = dialogChangeHourTimePicker.getCurrentHour();
                        Integer chosenMinute = dialogChangeHourTimePicker.getCurrentMinute();

                        TimeData timeChosen = new TimeData(chosenHour,chosenMinute);

                        switch (timeType){
                            case TimeConstants.TIME_BEGIN:
                                AccessData.getSearchData().setTimeBegin(timeChosen);
                                break;
                            case TimeConstants.TIME_END:
                                AccessData.getSearchData().setTimeEnd(timeChosen);
                                break;
                        }

                        listener.acceptHourChangeAction();

                        dismiss();

                    }
                }
        );

        dialogChangeHourButtonCancel.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        listener.cancelHourChangeAction();

                        dismiss();
                    }
                }

        );

        return builder.create();
    }

    public interface ListenerDialogChangeHour {

        void acceptHourChangeAction();// Eventos Botón Positivo
        void cancelHourChangeAction();// Eventos Botón Negativo
    }

    public void attachFragment(Fragment fragment) {
        try {
            listener = (ListenerDialogChangeHour) fragment;

        } catch (ClassCastException e) {
            throw new ClassCastException(
                    fragment.toString() +
                            " no implementó OnSimpleDialogListener");

        }
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

}
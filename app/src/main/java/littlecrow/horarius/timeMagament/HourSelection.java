package littlecrow.horarius.timeMagament;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import littlecrow.horarius.activities.selection.MainActivity;
import littlecrow.horarius.R;
import littlecrow.horarius.data.AccessData;

public class HourSelection extends Fragment implements AlertHourSelector.ListenerDialogChangeHour {

    private TextView selectionHourTextHourBegin;
    private TextView selectionHourTextHourEnd;

    private Button selectionHourButtonAllDay;

    public HourSelection() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.selection_hour, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {

        selectionHourTextHourBegin = getView().findViewById(R.id.selection_hour_text_hour_begin);
        selectionHourTextHourBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeHour(v,TimeConstants.TIME_BEGIN);
            }
        });
        selectionHourTextHourEnd = getView().findViewById(R.id.selection_hour_text_hour_end);
        selectionHourTextHourEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeHour(v,TimeConstants.TIME_END);
            }
        });

        selectionHourButtonAllDay = getView().findViewById(R.id.selection_hour_button_all_day);
        selectionHourButtonAllDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccessData.getSearchData().setTimeBegin(TimeUtils.getNewTimeDataMinimum());
                AccessData.getSearchData().setTimeEnd(TimeUtils.getNewTimeDataMaximum());
                setHourText();
            }
        });
        setHourText();

        MainActivity.buttonSearchEnabler();
    }

    public void changeHour(View view, Integer timeType){
        AlertHourSelector dialogChangeHour = new AlertHourSelector();
        dialogChangeHour.attachFragment(this);
        dialogChangeHour.setTimeType(timeType);
        dialogChangeHour.show(getFragmentManager(), "AlertHourSelector");
    }

    public void acceptHourChangeAction(){
        setHourText();

    }


    public void cancelHourChangeAction(){

    }

    private void setHourText(){
        selectionHourTextHourBegin.setText(AccessData.getSearchData().getTimeBegin().getTimeString());
        selectionHourTextHourEnd.setText(AccessData.getSearchData().getTimeEnd().getTimeString());
        runTimeDataChecker();
    }

    private void runTimeDataChecker(){
        Integer timeDataStatus = TimeUtils.consistentHourChecker();
        int hourBeginColor;
        int hourEndColor;
        boolean shouldShowToast = true;
        String toastMessage = "";
        switch (timeDataStatus){
            case TimeConstants.TIME_CORRECT:
                hourBeginColor = getResources().getColor(R.color.time_correct);
                hourEndColor = getResources().getColor(R.color.time_correct);
                shouldShowToast = false;
                break;
            case TimeConstants.TIME_BEGIN_OUT_LIMITS:
                hourBeginColor = getResources().getColor(R.color.time_wrong);
                hourEndColor = getResources().getColor(R.color.time_correct);
                toastMessage = getResources().getString(R.string.selection_hour_text_hour_out_of_limits);
                break;
            case TimeConstants.TIME_END_OUT_LIMITS:
                hourBeginColor = getResources().getColor(R.color.time_correct);
                hourEndColor = getResources().getColor(R.color.time_wrong);
                toastMessage = getResources().getString(R.string.selection_hour_text_hour_out_of_limits);
                break;
            case TimeConstants.TIME_BEGIN_END_OUT_LIMITS:
                hourBeginColor = getResources().getColor(R.color.time_wrong);
                hourEndColor = getResources().getColor(R.color.time_wrong);
                toastMessage = getResources().getString(R.string.selection_hour_text_hour_out_of_limits);
                break;
            case TimeConstants.TIME_BEGIN_END_WRONG_SELECTION:
                hourBeginColor = getResources().getColor(R.color.time_wrong);
                hourEndColor = getResources().getColor(R.color.time_wrong);
                toastMessage = getResources().getString(R.string.selection_hour_text_hour_incorrect_selection);
                break;
            default:
                hourBeginColor = getResources().getColor(R.color.time_wrong);
                hourEndColor = getResources().getColor(R.color.time_wrong);
                toastMessage = getResources().getString(R.string.selection_hour_text_hour_wtf);
                break;
        }

        if(shouldShowToast){
            Toast.makeText(getContext(), toastMessage,Toast.LENGTH_SHORT).show();
        }
        selectionHourTextHourBegin.setTextColor(hourBeginColor);
        selectionHourTextHourEnd.setTextColor(hourEndColor);
        MainActivity.buttonSearchEnabler();
    }
}


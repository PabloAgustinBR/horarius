package littlecrow.horarius.timeMagament;

public class TimeConstants {

    public static final int HOUR = 1;
    public static final int MINUTE = 2;

    public static final int REAL_MAXIMUM_HOUR = 3;
    public static final int REAL_MINIMUM_HOUR = 5;

    public static final int COMPARITION_MAXIMUM_HOUR = 24 + REAL_MAXIMUM_HOUR;
    public static final int COMPARITION_MINIMUM_HOUR = 5;

    public static final int MAXIMUM_MINUTE = 0;
    public static final int MINIMUM_MINUTE = 0;


    public static final int TIME_BEGIN = 1;

    public static final int TIME_END = 2;


    public static final int TIME_CORRECT = 1;

    public static final int TIME_BEGIN_OUT_LIMITS = 2;
    public static final int TIME_END_OUT_LIMITS = 3;
    public static final int TIME_BEGIN_END_OUT_LIMITS = 4;
    public static final int TIME_BEGIN_END_WRONG_SELECTION = 5;

}

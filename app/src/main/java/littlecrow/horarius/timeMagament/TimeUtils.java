package littlecrow.horarius.timeMagament;

import java.util.Calendar;

import littlecrow.horarius.data.AccessData;
import littlecrow.horarius.data.searchData.TimeData;

import static littlecrow.horarius.timeMagament.TimeConstants.*;

public class TimeUtils {

    public static TimeData timeDataMinimum = getNewTimeDataMinimum();
    public static TimeData timeDataMaximum = getNewTimeDataMaximum();


    public static void setInitialTimes() {
        Calendar beginTime = Calendar.getInstance();
        beginTime.add(Calendar.MINUTE, -30);
        TimeData timeBegin = new TimeData(beginTime);

        if (!isHourConsistent(timeBegin)) {
            timeBegin = getNewTimeDataMinimum();
        }

        TimeData timeEnd = new TimeData(timeBegin);
        //timeEnd.add(HOUR, 1);
        timeEnd.add(MINUTE, 90);

        if (!isHourConsistent(timeEnd)) {
            timeEnd = getNewTimeDataMaximum();
        }

        AccessData.getSearchData().setTimeBegin(timeBegin);
        AccessData.getSearchData().setTimeEnd(timeEnd);

    }

    public static Integer consistentHourChecker() {
        TimeData timeBegin = AccessData.getSearchData().getTimeBegin();
        TimeData timeEnd = AccessData.getSearchData().getTimeEnd();
        if (isHourConsistent(timeBegin)) {
            if (isHourConsistent(timeEnd)) {
                if (timeBegin.isTimeDataAfter(timeEnd)) {
                    return TIME_BEGIN_END_WRONG_SELECTION;
                }
            } else {
                return TIME_END_OUT_LIMITS;
            }
        } else {
                if(isHourConsistent(timeEnd)){
                    return TIME_BEGIN_OUT_LIMITS;

                }else{
                    return TIME_BEGIN_END_OUT_LIMITS;

                }
        }

        return TIME_CORRECT;
    }

    private static boolean isHourConsistent(TimeData timeData) {
        return timeData.isTimeDataBeforeOrEquals(timeDataMaximum)
                && timeData.isTimeDataAfterOrEquals(timeDataMinimum);
    }

    public static TimeData getNewTimeDataMaximum() {
        return new TimeData(REAL_MAXIMUM_HOUR, MAXIMUM_MINUTE);
    }

    public static TimeData getNewTimeDataMinimum() {
        return new TimeData(REAL_MINIMUM_HOUR, MINIMUM_MINUTE);
    }

}
